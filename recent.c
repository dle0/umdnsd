/*
 * Recent-response suppression.
 *
 * "a Multicast DNS responder MUST NOT (except in the one
 *  special case of answering probe queries) multicast
 *  a record on a given interface until at least one second has elapsed
 *  since the last time that record was multicast on that particular
 *  interface." [RFC 6762]
 *
 * This module keeps track of recording when RRs have been sent,
 * and judging if a record would be a duplicate.
 */

#include "respond.h"
#include "recent.h"

struct recent {
	struct timespec expire;	/* one second after send time */
	const struct rr *rr;
};

void
recent_record(const struct rr *rr, const struct query_info *qi)
{
	/* Don't do suppression on unicast queries */
	if (qi->unicast)
		return;
}

int
recent_is_duplicate(const struct rr *rr, const struct query_info *qi)
{
	/* Don't do suppression on unicast queries */
	if (qi->unicast)
		return 0;

	return 0;
}
