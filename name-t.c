
/* Test domain name label functions */

#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>

#include "name.c"

#define assert_eq(s1,s2) do { \
	const char *_s1 = (s1); \
	const char *_s2 = (s2); \
	if (strcmp(_s1, _s2) != 0) { \
		fprintf(stderr, "%s:%d: assert_eq(%s, %s) failed\n" \
				" s1 = <%s>\n" \
				" s2 = <%s>\n", \
				__FILE__, __LINE__, #s1, #s2, _s1, _s2); \
		abort(); \
	} \
    } while (0)

static void
test_name_size()
{
	const _DN char COM[] = { 3, 'c', 'o', 'm', 0 };
	const _DN char ROOT[] = { 0 };
	const _DN char INVALID[] = { 0xc0, 0 };

	assert(name_size(COM) == 5);
	assert(name_size(ROOT) == 1);
	assert(name_size(INVALID) == 0);
}

static void
test_lc()
{
	assert(lc('A') == 'a');
	assert(lc('a') == 'a');
	assert(lc('@') == '@');
	assert(lc('Z') == 'z');
	assert(lc('z') == 'z');
	assert(lc('{') == '{');
	assert(lc('[') == '[');
	assert(lc(' ') == ' ');
	assert(lc(0xff) == 0xff);
	assert(lc(0x00) == 0x00);
}

static void
test_name_store_repr()
{
	_DN char n1[NAME_MAX];
	const _DN char COM[] = {3,'c','o','m', 0};
	const _DN char FOO_COM[] = {3,'f','o','o', 3,'c','o','m', 0};
	const _DN char ROOT[] = {0};

	/* name_eq() compares some trival names correctly */
	assert(name_eq(ROOT, ROOT));
	assert(name_eq(FOO_COM, FOO_COM));
	assert(name_eq(COM, COM));
	assert(!name_eq(COM, FOO_COM));
	assert(!name_eq(FOO_COM, COM));
	assert(!name_eq(ROOT, FOO_COM));

	/* name_repr() represents some trivial names correctly */
	assert_eq(name_repr(ROOT), ".");
	assert_eq(name_repr(COM), "com.");
	assert_eq(name_repr(FOO_COM), "foo.com.");

	/* name_store() refuses relative names without an origin */
	assert(name_store(n1, NULL, "@") == -1);
	assert(name_store(n1, NULL, "foo") == -1);
	assert(name_store(n1, NULL, "foo.com") == -1);

	/* name_store() refuses empty names */
	assert(name_store(n1, NULL, "") == -1);
	assert(name_store(n1, ROOT, "") == -1);

	/* @ under . becomes . */
	assert(name_store(n1, ROOT, "@") == 0);
	assert(name_eq(n1, ROOT));

	/* @ under foo.com. becomes foo.com. */
	assert(name_store(n1, FOO_COM, "@") == 0);
	assert(name_eq(n1, FOO_COM));

	/* . under foo.com. becomes . */
	assert(name_store(n1, FOO_COM, ".") == 0);
	assert(name_eq(n1, ROOT));

	/* foo.com under . becomes foo.com. */
	assert(name_store(n1, ROOT, "foo.com") == 0);
	assert(name_eq(n1, FOO_COM));

	/* foo under com. becomes foo.com. */
	assert(name_store(n1, COM, "foo") == 0);
	assert(name_eq(n1, FOO_COM));

	/* com. under foo.com. stays com. */
	assert(name_store(n1, FOO_COM, "com.") == 0);
	assert(name_eq(n1, COM));

/* A name of length 62 chars */
#define SIXTY_TWO "1234567890" \
		  "1234567890" \
		  "1234567890" \
		  "1234567890" \
		  "1234567890" \
		  "1234567890" \
		  "12"

	/* test the limits of names */
	assert(name_store(n1, NULL, SIXTY_TWO "345.")==-1);
	assert(name_store(n1, NULL, SIXTY_TWO "34.")==-1);
	assert(name_store(n1, NULL, SIXTY_TWO "3.")==0);
	assert(n1[0] == 63);
	assert(name_size(n1) == 1+63 + 1);
	assert(name_store(n1, NULL, SIXTY_TWO ".")==0);
	assert(n1[0] == 62);
	assert(name_size(n1) == 1+62 + 1);

	/* Check for names longer than the 255 byte limit */
	assert(name_store(n1, NULL,
		"1234567890123456789012345678901."  /* 32 */
		"1234567890123456789012345678901."  /* 64 */
		"1234567890123456789012345678901."
		"1234567890123456789012345678901."  /* 128 */
		"1234567890123456789012345678901."
		"1234567890123456789012345678901."
		"1234567890123456789012345678901."
		"12345678901234567890123456789."  /* 254 + 1 (for .)  */
		) == 0);

	assert(name_store(n1, NULL,
		"1234567890123456789012345678901."  /* 32 */
		"1234567890123456789012345678901."  /* 64 */
		"1234567890123456789012345678901."
		"1234567890123456789012345678901."  /* 128 */
		"1234567890123456789012345678901."
		"1234567890123456789012345678901."
		"1234567890123456789012345678901."
		"123456789012345678901234567890."  /* 255 + 1 (for .)  */
		) == -1);

	/* name_store() understands escapes and spaces */
	assert(name_store(n1, NULL, "\\.\\ \\065 .") == 0);  /* "\. A ." */
	assert(n1[0] == 4);		/* 04<. A > 00 */
	assert(n1[1] == '.');
	assert(n1[2] == ' ');
	assert(n1[3] == 'a');
	assert(n1[4] == ' ');
	assert(n1[5] == 0);
	assert_eq(name_repr(n1), "\\.\\032a\\032.");
}

static void
test_name_pack()
{
	char pkt[1024];
	_DN char n1[NAME_MAX];
	_DN char n2[NAME_MAX];
	_DN char n3[NAME_MAX];
	_DN char n4[NAME_MAX];
	uint16_t off;
	uint16_t os[NAME_PACK_OFFSETS] = {0};

	name_store(n1, NULL, "foo.");

	off = 1;
	off = name_pack(pkt, sizeof pkt, off, n1, os);
	assert(off == 6); /* 1 +  1+3 + 1 */
	assert(memcmp(&pkt[1], "\3foo\0", 6-1) == 0);
	assert(os[0] == 1); /* foo. */
	assert(os[1] == 0);

	name_store(n2, NULL, "foo.bar.");
	off = name_pack(pkt, sizeof pkt, off, n2, os);
	assert(off == 15); /* 6 + 1+3 + 1+3 + 1 */
	assert(memcmp(&pkt[6], "\3foo\3bar\0", 15-6) == 0);
	assert(os[0] == 1);
	assert(os[1] == 6);  /* foo.bar. */
	assert(os[2] == 10); /* bar. */
	assert(os[3] == 0);

	name_store(n3, NULL, "new.foo.bar.");
	off = name_pack(pkt, sizeof pkt, off, n3, os);
	assert(off = 21); /* 15 + 1+3 + 2 */
	assert(memcmp(&pkt[15], "\3new\xc0\x06", 21-15) == 0);
	assert(os[3] == 15); /* new.foo.bar */
	assert(os[4] == 0);

	name_store(n4, NULL, "blu.foo.bar.");
	off = name_pack(pkt, sizeof pkt, off, n4, os);
	assert(off = 27); /* 21 + 1+3 + 2 */
	assert(memcmp(&pkt[21], "\3blu\xc0\x06", 27-21) == 0);
	assert(os[4] == 21); /* blu.foo.bar */
	assert(os[5] == 0);
}

static void
test_name_unpack()
{
	const char pkt[] = {
		0,1,2,3,4,5,6,7,8,9,10,11,       /*  0: 12 bytes padding */
		3,'f','o','o', 3,'b','a','r', 0, /* 12: 9 bytes uncompressed */
		3,'b','a','z', 0xc0,12,          /* 21: 13 bytes uncompressed */
		                                 /* 27: end */
	};
	_DN char name[NAME_MAX];

	/* can unpack valid packet */
	assert(name_unpack(pkt, 21, 12, name) == 21);
	assert(name_size(&pkt[12]) == 9);
	assert(name_size(name) == 9);
	assert(memcmp(name, &pkt[12], 9) == 0);

	/* can't unpack short packet */
	assert(!name_unpack(pkt, 20, 12, name));

	/* can't unpack from inside header */
	assert(!name_unpack(pkt, sizeof pkt, 0, name));

	/* can unpack compressed */
	assert(name_unpack(pkt, 27, 21, name) == 27);
	assert(name_size(name) == 13);
	assert(memcmp(name, "\3baz\3foo\3bar", 13) == 0);

	/* same again, but with skipped storage */
	assert(!name_unpack(pkt, sizeof pkt, 0, NULL));
	assert(!name_unpack(pkt, 20, 12, NULL));
	assert(name_unpack(pkt, 21, 12, NULL) == 21);
	assert(name_unpack(pkt, 27, 21, NULL) == 27);
}

static void
test_name_packed_eq()
{
	const char pkt[] = {
		0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19, /*fake hdr*/
		3,'b','a','r',              /* 20: bar.com. */
		3,'c','o','m',0,            /* 24: com. */
		3,'f','o','o', 0xc0,20,     /* 29: foo.<bar.com.> */
		1,'x', 0xc0,24,             /* 35: x.<com.> */
		1,'y', 0xc0,29,             /* 39: y.<foo.<bar.com.>> */
		3,'t'                       /* 43: <truncated> */
	};
	uint16_t a;

	/* com. matches 24(com.) in packet */
	a = 0;
	assert(name_packed_eq("\3com", pkt, sizeof pkt, 24, &a));
	assert(a == 29);
	/* same again but with NULL offset return */
	assert(name_packed_eq("\3com", pkt, sizeof pkt, 24, NULL));

	/* com.bar. does not match 24(com.) */
	a = 0;
	assert(!name_packed_eq("\3com\3bar", pkt, sizeof pkt, 24, &a));
	assert(a == 29);
	/* same again but with NULL offset return */
	assert(!name_packed_eq("\3com\3bar", pkt, sizeof pkt, 24, NULL));

	/* con. does not match 24(com.) */
	assert(!name_packed_eq("\3con", pkt, sizeof pkt, 24, &a));
	/* . does not match 24(com.) */
	assert(!name_packed_eq("", pkt, sizeof pkt, 24, &a));

	/* foo.bar.com matches 29(foo<.bar.com>) */
	a = 0;
	assert(name_packed_eq("\3foo\3bar\3com", pkt, sizeof pkt, 29, &a));
	assert(a == 35);
	/* foo.bar. does not match 29(foo<.bar.com>) */
	a = 0;
	assert(!name_packed_eq("\3foo\3bar", pkt, sizeof pkt, 29, &a));
	assert(a == 35);
	/* bar.com. does not match 29(foo<.bar.com>) */
	a = 0;
	assert(!name_packed_eq("\3bar\3com", pkt, sizeof pkt, 29, &a));
	assert(a == 35);

	/* 29(foo<.bar.com>) does not match some single letter differences */
	assert(!name_packed_eq("\2fo\3bar\3com", pkt, sizeof pkt, 29, NULL));
	assert(!name_packed_eq("\4fooo\3bar\3com", pkt, sizeof pkt, 29, NULL));
	assert(!name_packed_eq("\3foo\2ba\3com", pkt, sizeof pkt, 29, NULL));
	assert(!name_packed_eq("\3foo\4baar\3com", pkt, sizeof pkt, 29, NULL));
	assert(!name_packed_eq("\3foo\3bar\3com\1x", pkt, sizeof pkt, 29, NULL));

	/* fail match with malformed packets 43(t?) */
	a = 99;
	assert(!name_packed_eq("\1trx\3com", pkt, sizeof pkt, 43, &a));
	assert(a == 0);

}



int
main()
{
	test_lc();
	test_name_size();
	test_name_store_repr();
	test_name_pack();
	test_name_unpack();
	test_name_packed_eq();
	return 0;
}
