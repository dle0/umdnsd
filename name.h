#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

struct rr;

/*
 * Each DNS name is encoded as a sequence of non-empty labels terminating
 * with a single empty label, "". Each labels is at most 63 characters long,
 * and is encoded with a leading length byte. The total length of the encoded
 * name is at most 255 bytes. The type of this array is `_DN char[]`.
 *
 * DNS names can also be stored in a packet in a 'compressed' form,
 * but this name struct only ever holds a name in its 'uncompressed' form.
 * All names should be stored with lowercase characters to make comparison
 * easier.
 *
 * The 'empty' name (zero length) is never a valid name. The shortest
 * name is the root name consisting of the single terminating empty label.
 */
#define NAME_MAX 255
#define LABEL_MAX 63

#define _DN  /* tag indicating a label-encoded DN (not a C string) */

/* Returns the size of an uncompressed name, or
 * returns 0 if it was compressed, or would be longer than 255. */
uint8_t name_size(const _DN char *name);

#define NAME_ROOT_INIT		{ 0 }		/* empty name */

/* Compares two uncompressed names.
 * Returns true (nonzero) iff equal. */
int name_eq(const _DN char *n1, const _DN char *n2);

/*
 * Unpacks (decompresses) a name from a DNS packet.
 * The expanded name is lowercased and stored in *name_return,
 * which must be NULL or have NAME_MAX bytes of space.
 * When name_return is NULL, the stored name is simply skipped.
 * Returns the offset of the following byte in the packet, or
 * returns 0 on decoding error.
 */
uint16_t name_unpack(const char *pkt, uint16_t pktlen, uint16_t offset,
	_DN char *name_return);

/* Compares an uncompressed name n1 with a compressed name n2.
 *
 * If after_n2 is supplied, the offset after the compressed name n2 is
 * stored, regardless of match result. Zero is stored if the name was malformed.
 *
 * Returns 0 when unequal, or if the name was malformed.
 * Returns 1 when n1 matches the well-formed, packed name at offset_n2.
 */
int name_packed_eq(const _DN char *n1, const void *pkt, uint16_t pktlen,
	uint16_t offset_n2, uint16_t *after_n2);

/*
 * Creates a DNS name from a human-readable string representation.
 * Backslashes in the string are processed.
 * All uppercase characters are stored as lowercase.
 * Whitespace in the name is treated literally.
 * If the name is exactly "@", the whole name is replaced with the origin.
 * If the name did not terminate with a '.', the origin is appended.
 * The destination should be sized as `_DN char name_out[NAME_MAX]`.
 * Returns 0 on success, or
 * Returns -1 on error, sets n->len to zero and sets name_error to a message.
 */
int name_store(_DN char *name_out, const _DN char *origin_name, const char *str);

int name_storen(_DN char *name_out, const _DN char *origin_name,
	const char *str, unsigned str_len);

/*
 * Returns a human representation of an uncompressed DNS name.
 * Returns a pointer to static storage.
 */
const char *name_repr(const _DN char *name);

/*
 * Packs the DNS name into a packet buffer using compression.
 * This function uses a table of label offsets to assist with compression.
 * If the offsets table is NULL, no compression is performed.
 * The table is updated as new labels are stored.
 * The table should be initialized with a leading zero element on first use.
 * Returns the offset immediately after the packed name, or
 * returns 0 to indicate an error.
 */
#define NAME_PACK_OFFSETS 128
uint16_t name_pack(char *pkt, uint16_t pktsz, uint16_t offset,
	const _DN char *name,
	uint16_t offsets[static NAME_PACK_OFFSETS] /* or NULL pointer */);

/* Packs the data portion of an rr into a packet buffer.
 * Uses the same offsets table as name_pack().
 * Returns new offset at the end of packing, or
 * Returns 0 when packet buffer space is exhausted. */
uint16_t rr_pack_data(char *pkt, uint16_t pktsz, uint16_t offset,
	const struct rr *rr, uint16_t offsets[static NAME_PACK_OFFSETS]);

/* The last error encountered by name_store() */
extern const char *name_error;
