/*
 * Listen on an IPv6 mDNS multicast group
 */
#include <stdio.h>
#include <err.h>
#include <string.h>
#include <ifaddrs.h>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <net/if.h>
#include <unistd.h>

#include "mcast6.h"
#include "verbose.h"

#define IN6ADDR_MDNS_INIT \

static const struct in6_addr MDNS_ADDR6 = {   /* ff02::fb */
    .s6_addr = { 0xff, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFB }
};
static const in_port_t MDNS_PORT = 5353;

/* Joins the mDNS multicast group on the given interface.
 * Return 0 on success, -1 on error */
static int
mcast6_join(int fd, unsigned int ifindex, const char *ifname)
{
	struct ipv6_mreq mreq = {
		.ipv6mr_multiaddr = MDNS_ADDR6,
		.ipv6mr_interface = ifindex
	};
	int ret;
	char buf[INET6_ADDRSTRLEN];

	if (verbose)
		printf("IPV6_JOIN_GROUP %d (%s) %s\n", ifindex, ifname,
			inet_ntop(AF_INET6, &mreq.ipv6mr_multiaddr,
			buf, sizeof buf));

	ret = setsockopt(fd, IPPROTO_IPV6, IPV6_JOIN_GROUP, &mreq, sizeof mreq);

	if (ret == -1)
		warn("IPV6_JOIN_GROUP %d (%s) %s", ifindex, ifname,
			inet_ntop(AF_INET6, &mreq.ipv6mr_multiaddr,
			buf, sizeof buf));
	return ret;
}

/* Create an IPv6 multicast listener socket,
 * listening on the given interfaces (or on the system's
 * default interface if no interfaces are given).
 * Returns the socket file descriptor on success, or -1 on error. */
int
mcast6_socket(const char *ifname)
{
	int fd;
	struct sockaddr_in6 sa = {
		.sin6_family = AF_INET6,
		.sin6_port = htons(MDNS_PORT)
	};
	int val;
	unsigned int ifindex;

	if (strcmp(ifname, "default") == 0) {
		ifindex = 0;
	} else {
		ifindex = if_nametoindex(ifname);
		if (!ifindex) {
			warnx("%s: unknown interface", ifname);
			return -1;
		}
	}

	fd = socket(AF_INET6, SOCK_DGRAM, 0);
	if (fd == -1)
		err(1, "socket");

	if (mcast6_join(fd, ifindex, ifname) == -1) {
		close(fd);
		return -1;
	}

	if (ifindex) {
		/* Restrict sends to the interface */
		if (setsockopt(fd, IPPROTO_IPV6, IPV6_MULTICAST_IF, &ifindex,
		    sizeof ifindex) == -1)
			warnx("IPV6_MULTICAST_IF %s", ifname);
	}

#if 0
	/* Restrict multicast sends to one hop */
	val = 1;
	if (setsockopt(fd, IPPROTO_IPV6, IPV6_MULTICAST_HOPS, &val,
	    sizeof hops) == -1)
		warnx("IPV6_MULTICAST_HOPS %d", val);
#endif

	/* Set unicast hop limit so that local clients only can receive replies. */
	val = 1;
	if (setsockopt(fd, IPPROTO_IPV6, IPV6_UNICAST_HOPS, &val, sizeof val) == -1)
		warn("IPV6_UNICAST_HOPS");

	/* bind and share the mDNS port with other processes */
	val = 1;
	if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &val, sizeof val) == -1)
		warn("SO_REUSEADDR");
	val = 1;
	if (setsockopt(fd, SOL_SOCKET, SO_REUSEPORT, &val, sizeof val) == -1)
		warn("SO_REUSEPORT");
	if (bind(fd, (const struct sockaddr *)&sa, sizeof sa) == -1)
		err(1, "bind port %u", ntohs(sa.sin6_port));

	return fd;
}

int
mcast6_send(int fd, const void *data, unsigned int datalen)
{
	const struct sockaddr_in6 to = {
		.sin6_family = AF_INET6,
		.sin6_port = htons(MDNS_PORT),
		.sin6_addr = MDNS_ADDR6,
	};

	return sendto(fd, data, datalen, 0,
		(const struct sockaddr *)&to, sizeof to);
}
