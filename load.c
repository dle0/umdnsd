/*
 * Zone file loading
 *
 * RR types and their data formats are defined by various documents:
 *  RFC 1035 - CNAME, HINFO, MB, MD, MF, MG, MINFO, MR, MX, NULL,
 *             NS, PTR, SOA, TXT, A, WKS
 *  RFC 1183 - AFSDB, RP, X25, ISDN, RT
 *  RFC 1637 - NSAP
 *  RFC 1876 - LOC
 *  RFC 2535 - KEY, SIG
 *  RFC 2671 - OPT
 *  RFC 2782 - SRV
 *  RFC 2845 - TSIG
 *  RFC 4034 - DNSKEY, RRSIG, NSEC, DS
 *  RFC 3596 - AAAA
 *  RFC 3597 - unknown
 *
 * An RR's RDATA field may be up to 0xffff bytes long.
 */

#include <stdio.h>
#include <assert.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <err.h>
#include <errno.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include "zones.h"
#include "rr.h"
#include "name.h"
#include "verbose.h"

#define A_SIZE		4
#define AAAA_SIZE	16

static int load_file(const char *filename, const _DN char *origin);

/*
 * RDATA formats; a C string encodes the layout of a type's RDATA,
 * using a sequence of byte. This is used for both processing
 * zone files, and for printing RDATA.
 *
 *    a - IPv4 address
 *    A - IPv6 address
 *    u - uint32
 *    h - uint16
 *    n - DNS name (label sequence)
 *    t - text
 *    x - hexbinary                   must be last
 *    + - optional repeat             must be last
 */
static const struct table rr_rdata_format[] = {
	{ T_A,		"a" },
	{ T_AAAA,	"A" },
	{ T_SRV,	"hhhn" },
	{ T_MX,		"hn" },
	{ T_PTR,	"n" },
	{ T_CNAME,	"n" },
	{ T_NS,		"n" },
	{ T_TXT,	"t+" },
	{ T_HINFO,	"tt" },
	{}
};

/* Prints a RR, for human consumption. */
static void
rr_print(FILE *f, const _DN char *rr_name, const struct rr *rr)
{
	char buf[1024];
	const char *next_data;
	const char *data;
	uint32_t rr_len;
	uint16_t u16;
	uint32_t u32;
	uint8_t u8;
	unsigned int i;
	const char *fmt;
	const char *s;

	if (rr_name)
		fputs(name_repr(rr_name), f);
	fprintf(f, " %" PRIu32, rr->rr_ttl);

	s = table_to_str(rr_class_table, rr->rr_class);
	if (s) fprintf(f, " %s", s);
	else   fprintf(f, " CLASS%" PRIu32, rr->rr_class);

	s = table_to_str(rr_type_table, rr->rr_type);
	if (s) fprintf(f, " %s", s);
	else   fprintf(f, " TYPE%" PRIu32, rr->rr_type);

	fmt = table_to_str(rr_rdata_format, rr->rr_type);
	if (!fmt)
		fmt = "x";

	/* STEP safely sets `data` to point at next chunk of `n` bytes,
	 * decrementing `rr_len`. */
#	define STEP(data, n) do { \
		uint16_t _step; \
		(data) = next_data; \
		_step = (n); /* evaluate n after data */ \
		if (rr_len < _step) \
			goto error; \
		next_data += _step; \
		rr_len -= _step; \
	} while (0)
	next_data = rr->rr_data;
	rr_len = rr->rr_len;

	for (; *fmt; fmt++) {
again:
		switch (*fmt) {
		case 'a': /* IPv4 address */
			STEP(data, A_SIZE);
			fprintf(f, " %s", inet_ntop(AF_INET, data,
				buf, sizeof buf));
			break;
		case 'A': /* IPv6 address */
			STEP(data, AAAA_SIZE);
			fprintf(f, " %s", inet_ntop(AF_INET6, data,
				buf, sizeof buf));
			break;
		case 'h': /* uint16_t */
			STEP(data, sizeof u16);
			memcpy(&u16, data, sizeof u16);
			fprintf(f, " %" PRIu16, ntohs(u16));
			break;
		case 'u': /* uint32_t */
			STEP(data, sizeof u32);
			memcpy(&u32, data, sizeof u32);
			fprintf(f, " %" PRIu32, ntohl(u32));
			break;
		case 'n': /* DNS name */
			assert(rr_len <= NAME_MAX);
			STEP(data, name_size((const _DN char *)data));
			fprintf(f, " %s", name_repr((const _DN char *)data));
			break;
		case 't': /* text */
			STEP(data, 1);
			u8 = *data;
			STEP(data, u8);
			fputs(" \"", f);
			while (u8--) {
				char ch = *data++;
				if (ch < ' ' || ch == 0x7f)
					fprintf(f, "\\%03u", ch & 0xff);
				else if (ch == '\\' || ch == '"')
					fprintf(f, "\\%c", ch);
				else
					putc(ch, f);
			}
			putc('"', f);
			break;
		case 'x': /* unknown */
			fprintf(f, "\\# %" PRIu32, rr_len);
			i = 0;
			STEP(data, rr_len);
			while (data < next_data) {
				if ((i++ & 15) == 0) putc(' ', f);
				fprintf(f, "%02x", *data++);
			}
			break;
		case '+':
			if (!rr_len)
				break;
			fmt--;
			goto again;
		default:
			abort();
		}
	}
#undef STEP
	if (rr_len)
		fprintf(stderr, "rr too long\n");
	return;

error:
	fprintf(stderr, "rr too short\n");
}

/*
 * Note: rr_pack_data() is declared in name.h, but implemented here
 * because rr_rdata_format[] is private.
 */

uint16_t
rr_pack_data(char *pkt, uint16_t pktsz, uint16_t offset,
	const struct rr *rr, uint16_t offsets[static NAME_PACK_OFFSETS])
{
	const char *fmt = table_to_str(rr_rdata_format, rr->rr_type);
	uint16_t sz;
	const char *data = rr->rr_data;
	uint16_t len = rr->rr_len;
	int compress;

	/* RFC6762 18.14 says only compress some types */
	switch (rr->rr_type) {
	case T_NS:	case T_CNAME:
	case T_PTR:	case T_DNAME:
	case T_SOA:	case T_MX:
	case T_AFSDB:	case T_RT:
	case T_KX:	case T_RP:
	case T_PX:	case T_SRV:
	case T_NSEC:
		compress = 1;
		break;
	default:
		compress = 0;
	}

	if (!fmt)
		fmt = "x";
	for (; *fmt; fmt++) {
    again:
		switch (*fmt) {
		case 'a': sz = A_SIZE; goto raw;
		case 'A': sz = AAAA_SIZE; goto raw;
		case 'h': sz = sizeof (uint16_t); goto raw;
		case 'u': sz = sizeof (uint32_t); goto raw;
		case 'n':
			offset = name_pack(pkt, pktsz, offset,
				       (const _DN char *)data,
				       compress ? offsets : NULL);
			if (!offset)
				return 0;
			len = 0;
			break;
		case 't':
			assert(len);
			sz = pkt[offset] + 1;
			goto raw;
		case 'x':
			sz = len;
    raw:
			/* Copy sz bytes from rr_data into pkt */
			assert(sz <= len);
			if (offset + sz > pktsz)
				return 0;
			memcpy(pkt + offset, data, sz);
			data += sz;
			len -= sz;
			offset += sz;
			break;
		case '+':
			if (!len)
				break;
			fmt--;
			goto again;
		}
	}
	return offset;
}

/* Compare an rr's data against the <RDATA> at offset for equality.
 * The rr's class is used to decode the rdata. Names especially are used.
 * If after_return is supplied, stores the offset after <rlength,rdata> into it,
 * or stores 0 if the packet was malformed.
 * Returns 1 iff the packet data compares equal to the rr's data.
 * Returns 0 if the rdata was the wrong size or did not match. */
int
rr_eq_data(const char *pkt, uint16_t pktsz, uint16_t offset, uint16_t rdlength,
	const struct rr *rr, uint16_t *after_return)
{
	const char *fmt = table_to_str(rr_rdata_format, rr->rr_type);
	uint16_t rr_pos;
	uint16_t after;
	uint16_t pos;
	uint16_t posmax = offset + rdlength;
	uint8_t pkt_tlen, rr_tlen;
	int same;
	const _DN char *rr_name;

	pos = offset;
	rr_pos = 0;

	if (pos + rdlength > pktsz)
		goto truncated;

	for (same = 1; *fmt && same && pos < posmax; fmt++) {
		uint16_t sz;

		switch (*fmt) {
		case 'a': sz = A_SIZE; goto raw;
		case 'A': sz = AAAA_SIZE; goto raw;
		case 'u': sz = sizeof (uint32_t); goto raw;
		case 'h': sz = sizeof (uint16_t); goto raw;
		case 'x': sz = posmax - pos; goto raw;
    raw:
			/* Compare fixed-size elements by binary content */
			assert(rr_pos + sz <= rr->rr_len);
			if (pos + sz > posmax)
				goto truncated;
			same = memcmp(pkt + pos, rr->rr_data + rr_pos, sz) == 0;
			rr_pos += sz;
			pos += sz;
			break;
		case 't':
			if (fmt[1] == '+') {
				/* Treat t+ like x; i.e. exact binary match */
				fmt++;
				sz = posmax - pos;
				goto raw;
			}

			/* Compare single text string */
			pkt_tlen = (uint8_t)pkt[pos++]; /* OK because pos<posmax */
			if (pos + pkt_tlen > posmax)
				goto truncated;

			assert(rr_pos < rr->rr_len);
			rr_tlen = rr->rr_data[rr_pos++];
			assert(rr_tlen + rr_pos <= rr->rr_len);

			same = rr_tlen == pkt_tlen && memcmp(pkt + offset,
				rr->rr_data + rr_pos, rr_tlen) == 0;
			rr_pos += rr_tlen;
			pos += pkt_tlen;
			break;
		case 'n':
			/* Compare names */

			/* Note: The name in rr_data is never compressed */
			rr_name = rr->rr_data + rr_pos;
			same = name_packed_eq(rr_name, pkt, offset + rdlength,
				pos, &after);
			if (!after)
				goto truncated;
			rr_pos += name_size(rr_name);
			pos = after;
			break;
		default:
			abort();
		}
	}
	if (*fmt || rr_pos != rr->rr_len)
		same = 0;

	if (after_return)
		*after_return = offset + rdlength;
	return same;

truncated:
	if (after_return)
		*after_return = 0;
	return 0;
}


/*
 * Master file format parser context.
 */
struct context {
	const char *filename;
	unsigned int lineno;
	FILE *f;			/* opened input file */
	int nextch;			/* single char lookahead */
	const _DN char *name;		/* current value of @ */
	const _DN char *origin;
	_DN char name_buf[NAME_MAX];
	_DN char origin_buf[NAME_MAX];	/* set by $ORIGIN */
	uint32_t default_ttl;		/* set by $TTL and explicit mention */
	uint16_t default_class;		/* set by explicit mention */
	unsigned int paren;		/* parenthesis nesting depth */
};

static void
init_context(struct context *ctx, const char *filename, const _DN char *origin)
{
	ctx->filename = filename;
	ctx->lineno = 0;
	ctx->f = NULL;
	ctx->nextch = -1;
	ctx->name = NULL;
	ctx->origin = origin;
	ctx->default_ttl = 10;
	ctx->default_class = C_IN;
	ctx->paren = 0;
}

/* Prints a warning message, including context filename and line number. */
#define ctx_warn(ctx, ...) do { \
	const struct context *_ctx = (ctx); \
	fprintf(stderr, "%s:%u: ", _ctx->filename, _ctx->lineno); \
	warnx(__VA_ARGS__); \
    } while (0)

/* Exits while printing an error message, including context
 * filename and line number. */
#define ctx_fatal(ctx, ...) do { \
	const struct context *_ctx = (ctx); \
	fprintf(stderr, "%s:%u: ", _ctx->filename, _ctx->lineno); \
	errx(1, __VA_ARGS__); \
    } while (0)

/* Advances to next byte in the input file. */
static int
nextch(struct context *ctx)
{
	int ch = ctx->nextch;
	if (ch != -1)
		ctx->nextch = getc(ctx->f);
	if (ch == '\n')
		ctx->lineno++;
	return ch;
}

/* Pushes back a byte. This is the opposite of nextch(). */
static void
ctx_ungetc(struct context *ctx, int ch)
{
	if (ctx->nextch != -1)
		ungetc(ctx->nextch, ctx->f);
	ctx->nextch = ch;
}

/* Ignores raw chars until \n is eaten.
 * Useful for recovering to, or expecting immediate end of line.
 * Return 0 iff all ignored was whitespace */
static int
skip_to_eol(struct context *ctx)
{
	int dirty = 0;
	while (ctx->nextch != -1 && ctx->nextch != '\n') {
		if (!isspace(ctx->nextch))
			dirty = 1;
		nextch(ctx);
	}
	return dirty;
}

/* Skips over whitespace, handling parentheses.
 * Suitable for advancing between tokens.
 * Skips ' ', '\t' and comments, and processes parentheses.
 * Stops at '\n' if outside parentheses.
 * On return, the nextch will either be -1, '\n', or
 * a char other than " \t;()". */
static void
skip_white(struct context *ctx)
{
	while (ctx->nextch != -1) {
		if (ctx->paren && ctx->nextch == ')') {
			ctx->paren--;
		} else if (ctx->nextch == '(') {
			ctx->paren++;
		} else if (ctx->nextch == ';') {
			skip_to_eol(ctx);
			if (!ctx->paren)
				break;
		} else if (ctx->paren
			? !isspace(ctx->nextch)
			: !(ctx->nextch == ' ' || ctx->nextch == '\t')) {
				break;
		}
		nextch(ctx);
	}
}

/* Skips rest of a line, including any arguments within parentheses.
 * Suitable for recovery to the end of a line.
 * Leaves positioned at EOL or EOF, and always outside of any parentheses.
 * Returns 1 if non-empty text was skipped. */
static int
skip_rest(struct context *ctx)
{
	int dirty = 0;
	skip_white(ctx);
	while (ctx->paren && ctx->nextch != -1) {
		int ch = nextch(ctx);
		if (ch == '\\')
			ch = nextch(ctx);
		dirty = 1;
		skip_white(ctx);
	}
	if (ctx->paren) {
		ctx_warn(ctx, "unclosed (");
		ctx->paren = 0;
	}
	if (skip_to_eol(ctx))
		dirty = 1;
	return dirty;
}

/* Tests if a char could be part of word token. */
static int
isword(int ch)
{
	return ch != -1
	    && ch != '('
	    && ch != ')'
	    && ch != ';'
	    && !isspace(ch);
}

/*
 * Scans in a 'word' from the input.
 * A word is a token matching:  ([^();\s]|\\.)+
 * or a quoted string matching: "([^"\\]|\\.)*"
 * Backslashes in the word are retained in the output buffer.
 * Surrounding quotes are removed.
 * Returns 0 on success, or
 * returns -1 on error.
 */
static int
scan_word(char *word, size_t wordsz, struct context *ctx, const char *expected)
{
	int quote = 0;

	skip_white(ctx);

	if (!isword(ctx->nextch)) {
		ctx_warn(ctx, "%s", expected);
		return -1;
	}

	if (ctx->nextch == '"')
		quote = nextch(ctx);
	while (ctx->nextch != -1 &&
	       quote ? ctx->nextch != quote
	             : isword(ctx->nextch))
	{
		int escaped = (ctx->nextch == '\\');
		if (wordsz) {
			*word++ = ctx->nextch;
			wordsz--;
		}
		nextch(ctx);
		if (escaped) {
			if (ctx->nextch == -1) {
				ctx_warn(ctx, "eof");
				break;
			}
			if (wordsz) {
				*word++ = ctx->nextch;
				wordsz--;
			}
			nextch(ctx);
		}
	}
	if (quote) {
		if (ctx->nextch != quote) {
			ctx_warn(ctx, "unclosed \"");
			return -1;
		}
		nextch(ctx);
	}
	if (!wordsz) {
		ctx_warn(ctx, "too long");
		return -1;
	}
	*word = '\0';
	return 0;
}

/* Scans in a domain name from the input.
 * Special names like "@" are expanded to the origin.
 * Returns -1 on error. */
static int
scan_name(_DN char *name, struct context *ctx)
{
	char nametext[(4*LABEL_MAX+1)*4+1]; /* max valid name \177... */

	if (scan_word(nametext, sizeof nametext, ctx, "expected name") == -1)
		return -1;
	if (name_store(name, ctx->origin, nametext) == -1) {
		ctx_warn(ctx, "%s", name_error);
		return -1;
	}
	return 0;
}

/*
 * Consumes the rest of an empty line, eating the '\n' byte.
 * On return the nextch will be the first char of the next line, or EOF.
 * Returns 0 iff the line was empty or only had comments before '\n'.
 * Returns -1 on error, and recovers the input the start of the next line.
 */
static int
scan_eol(struct context *ctx)
{
	int ret = 0;

	if (skip_rest(ctx)) {
		ctx_warn(ctx, "unexpected data");
		ret = -1;
	}
	if (ctx->nextch == '\n')
		nextch(ctx);
	else {
		ctx_warn(ctx, "unexpected eof");
		ret = -1;
	}
	return ret;
}

/* Scans in a 32-bit integer from the input.
 * Suitable for general decimal integers.
 * Detects overflow.
 * Returns -1 on error.  */
static int
scan_u32(uint32_t *ret, struct context *ctx)
{
	uint32_t val = 0;
	int overflow = 0;

	skip_white(ctx);
	if (ctx->nextch == -1 || !isdigit(ctx->nextch)) {
		ctx_warn(ctx, "bad integer");
		return -1;
	}
	while (ctx->nextch != -1 && isdigit(ctx->nextch)) {
		uint32_t digit = nextch(ctx) - '0';
		if (val > (UINT32_MAX / 10) ||
		    (val == (UINT32_MAX / 10) && digit > (UINT32_MAX % 10)))
			overflow = 1;
		val = val * 10 + digit;
	}
	if (overflow) {
		ctx_warn(ctx, "integer overflow");
		*ret = UINT32_MAX;
		return -1;
	}

	*ret = val;
	return 0;
}

/* Scans in an IP address, AF_INET or AF_INET6 and appends to rdata.
 * Returns -1 on error. */
static int
scan_rr_ip(struct context *ctx, int af, struct rr **rrp)
{
	char word[INET6_ADDRSTRLEN];
	union {
		struct in_addr v4;
		struct in6_addr v6;
	} addr;

	if (scan_word(word, sizeof word, ctx, "expected IP address") == -1)
		return -1;
	if (inet_pton(af, word, &addr) != 1) {
		ctx_warn(ctx, "invalid IP address");
		return -1;
	}
	return rr_cat(rrp, &addr, af == AF_INET ? A_SIZE : AAAA_SIZE);
}

/* Scans in a domain name and appends its packed form to rdata.
 * Returns -1 on error. */
static int
scan_rr_name(struct context *ctx, struct rr **rrp)
{
	_DN char name[NAME_MAX];

	if (scan_name(name, ctx) == -1)
		return -1;
	return rr_cat(rrp, name, name_size(name));
}

/* Scans in an unsigned integer and appends to rdata.
 * Returns -1 on error. */
static int
scan_rr_u16(struct context *ctx, struct rr **rrp)
{
	uint32_t u32;
	uint16_t u16;

	if (scan_u32(&u32, ctx) == -1)
		return -1;
	if (u32 > 0xffff) {
		ctx_warn(ctx, "integer too big for 16 bits");
		return -1;
	}
	u16 = htons(u32);
	return rr_cat(rrp, &u16, sizeof u16);
}

/* Scans in an unsigned integer and appends to rdata.
 * Returns -1 on error. */
static int
scan_rr_u32(struct context *ctx, struct rr **rrp)
{
	uint32_t u32;

	if (scan_u32(&u32, ctx) == -1)
		return -1;
	u32 = htonl(u32);
	return rr_cat(rrp, &u32, sizeof u32);
}

/* Converts first 3 decimal digits of s into an output byte.
 * Returns 0 iff s did not start with a three-digit decimal number. */
static int
from_decimal(const char *s, char *out)
{
	if (isdigit(s[0]) &&
	    isdigit(s[1]) &&
	    isdigit(s[2]))
	{
		*out = (s[0] - '0') * 100 +
		       (s[1] - '0') * 10 +
		       (s[2] - '0');
		return 1;
	} else {
		return 0;
	}
}

/* Scans generic quoted text, performs escape processing, and
 * appends to rdata.
 * Return -1 on error. */
static int
scan_rr_text(struct context *ctx, struct rr **rrp)
{
	char text[1 + 255 * 4 + 1];
	char *s, *t, ch;
	unsigned int len;

	if (scan_word(text + 1, sizeof text - 1, ctx, "expected text") == -1)
		return -1;

	/* in-place unescape */
	s = t = text + 1;
	while ((ch = *s++)) {
		if (ch == '\\' && from_decimal(s, t))
			t++, s += 3;
		else if (ch == '\\' && *s)
			*t++ = *s++;
		else
			*t++ = ch;
	}
	len = t - (text + 1);
	if (len > 255) {
		ctx_warn(ctx, "text string longer than 255 chars");
		return -1;
	}
	text[0] = len;
	return rr_cat(rrp, text, len + 1);
}

/* Scans in a single hex digit from the input.
 * Intermediate whitespace is skipped.
 * Returns -1 if the input was not a hex digit. */
static int
scan_xdigit(struct context *ctx)
{
	int ch;

	skip_white(ctx);
	if (ctx->nextch == -1 || !isxdigit(ctx->nextch)) {
		ctx_warn(ctx, "expected hexadecimal digit");
		return -1;
	}
	ch = nextch(ctx);
	return ch >= 'a' ? ch - 'a' + 10 :
	       ch >= 'A' ? ch - 'A' + 10 : ch - '0';
}

/* Scans binary RDATA form \# <len> <hex>, and appends to rdata.
 * Returns -1 if the input was not in valid \# form.  */
static int
scan_rr_hex(struct context *ctx, struct rr **rrp)
{
	uint32_t len;
	char *data;

	skip_white(ctx);
	if (nextch(ctx) != '\\' || nextch(ctx) != '#') {
		ctx_warn(ctx, "expected \\#");
		return -1;
	}
	skip_white(ctx);
	if (scan_u32(&len, ctx) == -1)
		return -1;
	data = rr_grow(rrp, len);
	if (!data)
		return -1;
	while (len--) {
		int hi, lo;
		if ((hi = scan_xdigit(ctx)) == -1)
			return -1;
		if ((lo = scan_xdigit(ctx)) == -1)
			return -1;
		*data++ = hi << 4 | lo;
	}
	return 0;
}


/* Scans a resource record definition from the input,
 * and allocates an rr in memory.
 *
 * The input is expected to be of the form:
 *    [ttl] [class] type rdata \n
 *    [class] [ttl] type rdata \n
 *
 * Comments and parentheses in the input are ignored.
 *
 * Returns the malloc'd rr on success, or NULL on failure.
 */
static struct rr *
scan_rr(struct context *ctx)
{
	uint32_t ttl = ctx->default_ttl;
	uint16_t class_ = ctx->default_class;
	int have_ttl = 0;
	int have_class = 0;
	char word[32];
	static struct rr rr, *rrd;
	const char *fmt;

	memset(&rr, 0, sizeof rr);

	skip_white(ctx);
	if (ctx->nextch != -1 && isdigit(ctx->nextch)) {
		/* . ttl [class] type rdata */
		if (scan_u32(&ttl, ctx) == -1)
			return NULL;
		have_ttl = 1;
	}
	if (scan_word(word, sizeof word, ctx, "expected class or type") == -1)
		return NULL;
	if (sscanf(word, "CLASS%" SCNu16, &class_) == 1 ||
	    table_to_val16(rr_class_table, word, &class_))
	{
		have_class = 1;
		skip_white(ctx);
		if (!have_ttl && ctx->nextch != -1 && isdigit(ctx->nextch)) {
			/* class . ttl type rdata */
			if (scan_u32(&ttl, ctx) == -1)
				return NULL;
		}
		if (scan_word(word, sizeof word, ctx, "expected type") == -1)
			return NULL;
	}
	fmt = NULL;
	if (sscanf(word, "TYPE%" SCNu16, &rr.rr_type) == 1 ||
	    table_to_val16(rr_type_table, word, &rr.rr_type)) {
		fmt = table_to_str(rr_rdata_format, rr.rr_type);
	} else {
		ctx_warn(ctx, have_ttl ? "unknown type or class after ttl"
		                      : "unknown type");
		return NULL;
	}

	rr.rr_ttl = ttl;
	rr.rr_class = class_;

	/* Update context defaults to "last explicitly stated values". */
	if (have_ttl)
		ctx->default_ttl = ttl;
	if (have_class)
		ctx->default_class = class_;

	if (!fmt)
		fmt = "x";

	/* If the lookahead is \# then override the format to expect it. */
	skip_white(ctx);
	if (ctx->nextch == '\\') {
		int la;
		(void) nextch(ctx);
		la = ctx->nextch;
		ctx_ungetc(ctx, '\\');
		if (la == '#')
			fmt = "x";
	}

	rrd = malloc(sizeof (struct rr));
	*rrd = rr;
	rrd->rr_len = 0;
	for (; *fmt; fmt++) {
		int ret;
	again:	switch (*fmt) {
		case 'a': ret = scan_rr_ip(ctx, AF_INET, &rrd); break;
		case 'A': ret = scan_rr_ip(ctx, AF_INET6, &rrd); break;
		case 'h': ret = scan_rr_u16(ctx, &rrd); break;
		case 'u': ret = scan_rr_u32(ctx, &rrd); break;
		case 'n': ret = scan_rr_name(ctx, &rrd); break;
		case 't': ret = scan_rr_text(ctx, &rrd); break;
		case 'x': ret = scan_rr_hex(ctx, &rrd); break;
		case '+': skip_white(ctx);
			  if (ctx->nextch == '\n' || ctx->nextch == -1)
				break;
			  fmt--; goto again;
		default: abort();
		}
		if (ret == -1 || !rrd) {
			free(rrd);
			return NULL;
		}
	}
	return rrd;
}

/* Scan in and handle a directive such as $ORIGIN, $TTL, $INCLUDE.
 * Return -1 on directive processing error.  */
static int
scan_directive(struct context *ctx)
{
	char word[32];

	if (scan_word(word, sizeof word, ctx, "expected $directive") == -1)
		return -1;

	if (strcmp(word, "$ORIGIN") == 0) {
		if (scan_name(ctx->origin_buf, ctx) == -1)
			return -1;
		ctx->origin = ctx->origin_buf;
		if (verbose)
			printf("$ORIGIN %s\n", name_repr(ctx->origin));
		return 0;
	}

	if (strcmp(word, "$TTL") == 0) {
		if (scan_u32(&ctx->default_ttl, ctx) == -1)
			return -1;
		if (verbose)
			printf("$TTL %" PRIu32 "\n", ctx->default_ttl);
		return 0;
	}

	if (strcmp(word, "$INCLUDE") == 0) {
		char path[FILENAME_MAX];
		_DN char name[NAME_MAX];
		const _DN char *origin;

		if (scan_word(path, sizeof path, ctx, "expected filename") ==-1)
			return -1;
		/* Should the path be escape-expanded? */
		skip_white(ctx);
		origin = ctx->origin;
		if (ctx->nextch != '\n') {
			if (scan_name(name, ctx) == -1)
				return -1;
			origin = name;
		}
		if (verbose)
			printf("$INCLUDE %s %s\n", path, name_repr(origin));
		return load_file(path, origin);
	}

	ctx_warn(ctx, "unknown directive");
	return -1;
}

/* Open and load all the zones from a file.
 * The $ORIGIN defaults to that given.
 * All resources loaded are stored via zones_store().
 * Files may recursively include other zone files.
 * Returns -1 on error. */
static int
load_file(const char *filename, const _DN char *origin)
{
	struct context ctx;
	int ret = 0;
	struct rr *rr;

	init_context(&ctx, filename, origin);

	ctx.f = fopen(filename, "r");
	if (!ctx.f) {
		warn("%s", filename);
		return -1;
	}
	ctx.nextch = getc(ctx.f);

	/* Process each line. */
	while (ctx.nextch != -1) {
		int leading_space = (ctx.nextch == ' ' || ctx.nextch == '\t');

		if (ctx.nextch == '$') {
			if (scan_directive(&ctx) == -1)
				goto line_error;
			goto line_end;
		}

		skip_white(&ctx);
		if (ctx.nextch == '\n' || ctx.nextch == -1)
			goto line_end;			/* blank line */

		/* Scan in a name iff the line had no leading space. */
		if (!leading_space && scan_name(ctx.name_buf, &ctx) == -1)
			goto line_error;
		ctx.name = ctx.name_buf;

		rr = scan_rr(&ctx);
		if (!rr)
			goto line_error;

		if (rr->rr_type == T_TXT && rr->rr_len == 0)
			ctx_warn(&ctx, "zero-length TXT forbidden by DNS-SD");

		/* Check for resource entries before a name was provided. */
		if (!ctx.name) {
			ctx_warn(&ctx, "missing name");
			rr_free(rr);
			goto line_error;
		}

		if (verbose) {
			printf("+ ");
			rr_print(stdout, ctx.name, rr);
			printf("\n");
		}
		if (zones_store(ctx.name, rr) == -1)
			ret = -1;
line_end:
		/* Expect the rest of the line to be blank. */
		if (scan_eol(&ctx) == -1)
			ret = -1;
		continue;

line_error:
		/* After an error silently skip to the end of the line. */
		(void) skip_rest(&ctx);
		if (ctx.nextch == '\n')
			nextch(&ctx);
		ret = -1;
	}
	fclose(ctx.f);
	return ret;
}

int
zones_load(const char *filename)
{
	return load_file(filename, (const _DN char *)"\005local");
}
