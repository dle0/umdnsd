#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#include <arpa/nameser.h>

#include "zones.h"
#include "rr.h"
#include "verbose.h"

static char tmp_zonefile[FILENAME_MAX];

static void
cleanup_tmp_zonefile()
{
	unlink(tmp_zonefile);
}

static void
init_once_tmp_zonefile()
{
	if (tmp_zonefile[0])
		return;
	snprintf(tmp_zonefile, sizeof tmp_zonefile, "/tmp/zone.%d", getpid());
	atexit(cleanup_tmp_zonefile);
}

/* Set content of the tmp_zonefile file */
static void
set_tmp_zonefile(const char *text)
{
	FILE *f;

	init_once_tmp_zonefile();
	f = fopen(tmp_zonefile, "w");
	assert(f);
	assert(fputs(text, f) != EOF);
	fclose(f);

	if (verbose) {
		const char *s;
		int eol = 1;
		for (s = text; *s; s++) {
			if (eol) putchar('|');
			putchar(*s);
			eol = (*s == '\n');
		}
	}
}


static void
test_zones_load()
{
	const struct zone_rr *zrr;

	verbose = 1;

	/* A simple zone file loads */
	set_tmp_zonefile(
		"abc. IN A 1.2.3.4\n"
	);
	assert(zones_load(tmp_zonefile) == 0);

	zrr = zones_lookup("\003abc\0");
	assert(zrr);
	assert(!zrr->next);
	assert(zrr->rr.rr_type == T_A);
	assert(zrr->rr.rr_len == 4);
	assert(memcmp(zrr->rr.rr_data, "\001\002\003\004", 4) == 0);
	zones_clear();
	assert(!zones_lookup("\3abc\0"));

	/* More complicated zone file */
	set_tmp_zonefile(
		"$ORIGIN com.\n"
		"foo TXT \"text\" \"\"; comment\n"
		"foo 99 SRV 1 2 3 baz\n"
		"\n"
		"bar PTR ( ; comment \n"
		"          foo.\n"
		" ) ; end\n"
	);
	assert(zones_load(tmp_zonefile) == 0);

	assert(!zones_lookup("\003foo\0"));
	/* <foo.com. TXT "text" ""> */
	zrr = zones_lookup("\003foo\003com\0");
	assert(zrr);
	assert(zrr->rr.rr_class == C_IN);
	assert(zrr->rr.rr_type == T_TXT);
	assert(zrr->rr.rr_len == 6);
	assert(memcmp(zrr->rr.rr_data, "\004text\000", 6) == 0);
	zrr = zrr->next;
	/* <foo.com. 99 SRV 1 2 3 baz.com.> */
	assert(zrr);
	assert(zrr->rr.rr_class == C_IN);
	assert(zrr->rr.rr_type == T_SRV);
	assert(zrr->rr.rr_ttl == 99);
	assert(zrr->rr.rr_len == 15); /* 2+2+2+(4+4+1) */
	assert(memcmp(zrr->rr.rr_data,
		"\0\1" "\0\2" "\0\3" "\003baz\003com\0", 15) == 0);
	assert(!zrr->next);
	/* <bar.com. PTR foo.> */
	zrr = zones_lookup("\003bar\003com\0");
	assert(zrr);
	assert(zrr->rr.rr_type == T_PTR);
	assert(zrr->rr.rr_len == 5);
	assert(memcmp(zrr->rr.rr_data, "\003foo\0", 5) == 0);

	zones_clear();
}

static void
test_rr_eq_data()
{
	const struct zone_rr *zrr;
	uint16_t after;

	static const char pkt[] = {
		0,1,2,3,4,5,6,7,8,9,10,11,

		3,'f','o','o',3,'c','o','m',0,		/* 12:foo. 16:com. */
		    0,T_A, 0,C_IN,			/* 21: A IN */
		    0,0,0,127,				/* 25: ttl 127 */
		    0,4, 1,2,3,4,			/* 29: # 4 01020304 */
							/* 35: */

		3,'b','a','r',0xc0,16,			/* 35:bar.[com.] */
		    0,T_CNAME, 0,C_IN,			/* 41: CNAME IN */
		    0,0,0,127,				/* 45: ttl 127 */
		    0,2, 0xc0,12,			/* 49: # 2 [foo.com.] */
							/* 53: */
	};

	set_tmp_zonefile(
		"foo.com. IN A 1.2.3.4\n"
		"foo.net. IN A 1.2.3.5\n"
		"bar.com. IN CNAME foo.com.\n"
	);
	assert(zones_load(tmp_zonefile) == 0);

	/* Comparing A 1.2.3.4 against A 1.2.3.4 works */
	zrr = zones_lookup("\3foo\3com"); /* IN A 1.2.3.4 */
	after = 0;
	assert(rr_eq_data(pkt, sizeof pkt, 29 + 2, 4, &zrr->rr, &after) == 1);
	assert(after == 35);

	/* Comparing A 1.2.3.5 against A 1.2.3.4 fails, but
	 * maintains the 'after' offset */
	zrr = zones_lookup("\3foo\3net"); /* IN A 1.2.3.5 */
	after = 0;
	assert(rr_eq_data(pkt, sizeof pkt, 29 + 2, 4, &zrr->rr, &after) == 0);
	assert(after == 35);

	/* Comparing the CDATA compressed string against the uncompressed bar.com.
	 * name (foo.com) works */
	zrr = zones_lookup("\3bar\3com");
	after = 0;
	assert(rr_eq_data(pkt, sizeof pkt, 49 + 2, 2, &zrr->rr, &after) == 1);
	assert(after == 53);

	/* Same but comparing with the 6 byte bar.com fails */
	after = 0;
	assert(rr_eq_data(pkt, sizeof pkt, 35, 6, &zrr->rr, &after) == 0);
	assert(after == 41);

	zones_clear();
}

int
main()
{

	test_zones_load();
	test_rr_eq_data();
	exit(0);
}
