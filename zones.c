/*
 * Zone database; memory management.
 */

#include <stdio.h>
#include <assert.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <err.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include "zones.h"
#include "rr.h"
#include "name.h"
#include "verbose.h"

/* A zone is a DNS name with a list of resource records (RRs) */
struct zone {
	struct zone_rr *first_rr;
	struct zone_rr *last_rr;
	struct zone *next;
	_DN char name[];
};

static struct zone *zones; /* All known zones */

static void
zone_free(struct zone *z)
{
	struct zone_rr *r;
	struct zone_rr *rnext;

	rnext = z->first_rr;
	while ((r = rnext)) {
		rnext = r->next;
		free(r);
	}
	/* z->first_rr = z->last_rr = NULL; */
}

void
zones_clear(void)
{
	struct zone *z;

	while ((z = zones)) {
		zones = z->next;
		zone_free(z);
	}
}

struct zone **
zones_find(const _DN char *name)
{
	struct zone **zp;

	for (zp = &zones; *zp; zp = &(*zp)->next)
		if (name_eq(name, (*zp)->name))
			break;
	return zp;
}

struct zone_rr *
zones_lookup(const _DN char *name)
{
	struct zone *z;

	z = *zones_find(name);
	return z ? z->first_rr : NULL;
}

static struct zone *
zone_new(const _DN char *name)
{
	struct zone *z;
	unsigned int namesz = name_size(name);

	z = malloc((sizeof *z) + namesz);
	if (!z) {
		warn("malloc");
		return NULL;
	}
	memcpy(z->name, name, namesz);
	z->first_rr = z->last_rr = NULL;
	z->next = NULL;
	return z;
}

static struct zone_rr *
zone_rr_new(const _DN char *name, const struct rr *rr)
{
	struct zone_rr *zr;
	size_t rrsz = rr_size(rr);

	zr = malloc((sizeof *zr) - (sizeof zr->rr) + rrsz);
	if (!zr)
		return NULL;
	memset(&zr->time, 0, sizeof zr->time);
	zr->name = name;
	memcpy(&zr->rr, rr, rrsz);
	return zr;
}

int
zones_store(const _DN char *name, const struct rr *rr)
{
	struct zone **zp;
	struct zone_rr *zr;

	zp = zones_find(name);
	if (!*zp) {
		*zp = zone_new(name);
		if (!*zp)
			return -1;
	}

	/* The name pointer belongs to *zp, but can be shared
	 * with *zr because the latter has a shorter lifetime. */
	zr = zone_rr_new((*zp)->name, rr);
	if (!zr)
		return -1;

	if ((*zp)->last_rr)
		(*zp)->last_rr->next = zr;
	else
		(*zp)->first_rr = zr;
	(*zp)->last_rr = zr;
	zr->next = NULL;
	return 0;
}

const _DN char *
zones_name_next(void **iter)
{
	struct zone **zonep;

	zonep = (struct zone **)*iter;
	if (!zonep)
		zonep = &zones;
	if (!*zonep) {
		*iter = NULL;
		return NULL;
	}
	*iter = &(*zonep)->next;
	return (*zonep)->name;
}
