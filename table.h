#pragma once

#include <inttypes.h>

/* Simple lookup table entry.
 * The last entry in a table[] must be {0,NULL}. */
struct table {
	unsigned int value;
	const char *str;
};

/* Searches a table for the string, ignoring case, and stores the
 * associated value in *value_ret.
 * Returns 1 on success, or 0 if not found. */
int table_to_val(const struct table *t, const char *str,
	unsigned int *value_ret);

/* Searches a table for the string, ignoring case, and stores the
 * associated value in *value_ret, truncated to 16 bits.
 * Returns 1 on success, or 0 if not found. */
int table_to_val16(const struct table *t, const char *str,
	uint16_t *value_ret);

/* Searches a table for the value.
 * Returns the accompanying string, or
 * returns NULL if the value was not found. */
const char *table_to_str(const struct table *t, unsigned int value);
