#pragma once

struct query_info;
struct rr;

/* Record the recent sending of a resource record */
void recent_record(const struct rr *rr, const struct query_info *qi);

/* Test if sending a resource record would count as a
 * 'recent' duplicate.  */
int recent_is_duplicate(const struct rr *rr, const struct query_info *qi);

/* Forget all RRs sent */
void recent_reset(void);
