/* Lookup table tests */
#include <assert.h>
#include "table.c"

int
main()
{
	static const struct table T[] = {
		{ 0, "zero" },
		{ 2, "two" },
		{ 1, "one" },
		{}
	};
	unsigned int val;
	const char *s;

	val = 99;

	assert(!table_to_val(T, "impossible", &val));
	assert(val == 99);

	assert(table_to_val(T, "zeRo", &val));
	assert(val == 0);
	assert(table_to_val(T, "ONE", &val));
	assert(val == 1);
	assert(table_to_val(T, "two", NULL));

	assert((s = table_to_str(T, 0)));
	assert(strcmp(s, "zero") == 0);
	assert((s = table_to_str(T, 1)));
	assert(strcmp(s, "one") == 0);
	assert(!table_to_str(T, 99));

	return 0;
}
