
# umdnsd - micro mDNS server

*umdnsd* is a small mDNS responder.
It is light on dependencies and resources.

## Quick start 1: myhost.local

    $ umdnsd eth0

This answers all address queries heard on eth0 for "myhost.local",
assuming your hostname is "myhost". No special privileges are needed.

## Quick start 2: Providing services

Services are easily provided:

1. Create umdnsd.db:
```
$AUTO local
"My SSH server"._ssh._tcp.local.  IN SRV 0 5 22 @
"My web server"._http._tcp.local. IN SRV 0 5 80 @
                                  IN TXT "page=/index.html"
```
2. ```$ umdnsd eth0 umdnsd.db```

If you change the zone file content, send a HUP to *umdnsd* to reload.

## Usage

    umdnsd [-46v] interface [zonefile ...]

* -4 and -6 options mean use IPv4-only or IPv6-only, respectively.
* -v enables verbose operation

## Zone files

The zone files served are in standard RFC 1035 master file format.

    domain.name [ttl] [IN] type data
    $INCLUDE "filename"
    $ORIGIN domain.name
    $TTL ttl
    ; comment

Note that the origin variable (initially `local` but changed with `$ORIGIN`)
is appended to any name not terminated by a dot.
The special name `@` expands to the current origin.

*umdnsd* understands the following resource types:
A AAAA CNAME MX NS PTR SRV TXT.
Other resource types can be specified using TYPEnn and `\#`.

### `$AUTO` directive

`$AUTO` is a non-standard directive
that automatically generates A, AAAA and PTR
records for the interface's addresses.

    $AUTO [name][.domain]

If the _name_ part is omitted, the system hostname is used.
When the _domain_ part is omitted, the origin is used (default ".local").
The `$AUTO` directive also sets the origin to the selected name.
This means the special name `@` can then be used for the system's domain name:

    $AUTO
    My\032Printer._printer._tcp.local. SRV 0 5 515 @
      TXT "path=/mysrv"

## Defaults

The default TTL is 10 seconds, and the default class is IN.
The initial origin is `.local`.
If no zone file is given, *umdnsd* uses the internal default
of `$AUTO`.

## DNS-SD (zeroconf) automatic PTR entries

When serving SRV records of the form:

    <label>._<label>._tcp.<label>
    <label>._<label>._udp.<label>

then *umdnsd* will automatically also answer the corresponding
DNS-SD PTR requests so as to support zeroconf browsing.

## Operation

On startup, *umdnsd* joins IPv4 multicast group _224.0.0.252_
and IPv6 multicast group_ff02::fb_ on the named interface,
and binds UDP port 5353.

Then (and on every SIGHUP) *umdnsd* reads into memory the
specified zone files (or simulated $AUTO entry).

*umdnsd* waits to receive question packets on the interface
and checks to see if it can answer any of the questions.
DNS-SD PTR requests are also expanded internally to see if they
can be answered with SRV records.

*umdnsd* ignores mulicast questions that it cannot answer.

When the source port of the question is not 5353, *umdnsd*
treats it as a unicast request and responds with a direct
UDP reply, further indicating NXDOMAIN when it is unable
to answer any question.  This makes it possible to use
tools such as *dig* to query *umdnsd* directly.

All response packets from *umdnsd* have a restricted IP TTL
(hop limit) to prevent them from travelling beyond the local link.

## Standards

* [RFC 1035](https://tools.ietf.org/html/rfc1035)
  Domain names - implementation and specification,
  Master file format
* [RFC 3597](https://tools.ietf.org/html/rfc3597)
  `\#` RDATA form in zone files
* [RFC 6762](https://tools.ietf.org/html/rfc6762)
  Multicast DNS (mDNS)
* [RFC 6763](https://tools.ietf.org/html/rfc6763)
  DNS-Based Service Discover (DNS-SD)

