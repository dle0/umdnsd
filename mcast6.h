#pragma once

/* Creates a mDNS IPv6 socket listening on the
 * given interfaces; or the default system interface
 * if none are given.
 * Returns a FD, or -1 on error. */
int mcast6_socket(const char *ifname);


/* Sends a mDNS packet to the IPv6 mDNS multicast address.
 * Returns number of bytes sent, or -1 on error. */
int mcast6_send(int fd, const void *data, unsigned int datalen);
