/*
 * This test runs an external script of recv/sent packet assertions,
 * calling and checking mdns_respond() in a simulated environment.
 *
 * The format of the script is line-oriented blocks of the form:
 *
 *    hostname <name>                    ; sets the system hostname
 *    sleep <int> [m]                    ; advance virtual clock in seconds
 *    ttl <int>                          ; default unspec'd ttl for recv/sent
 *
 *    ifaddr clear                       ; clears the virtual interface's addrs
 *    ifaddr <ipv4|ipv6>                 ; adds an IPv4/IPv6 address
 *
 *    zones clear                        ; clears the zone database
 *    zones {                            ; extra zones to load
 *        <masterfile records>           ; literal text of zone db to load
 *                                       ; (leading TAB is omitted!)
 *    }                                  ; (closing brace is on its own line)
 *
 *    recv {
 *        <packet>                       ; simulate receipt of packet
 *    }                                  ; it calls mdns_respond()
 *    sent {
 *        <packet>                       ; verifies mdns_respond()'s output
 *    }
 *    sent nothing                       ; verifies no response was sent
 *
 *    # comment                          ; comments and blank lines ignored
 *
 * A <packet> is a compact representation of a DNS packet used for both
 * generating a DNS packet or comparing a packet.
 * recv{} generates a packet for umdnsd, sent{} verifies the response.
 *
 * A <packet> is a sequence of lines, in the following order:
 *
 *        id <int>
 *        flags <[!](qr|aa|tc|rd|ra)> [(opcode=<int>|QUERY|IQUERY|STATUS)]
 *                                  [(rcode=<int>|NOERROR|SERVFAIL|...)]
 *        QU <name> [<class>[^]] <type>[^]
 *        AN <name> [<ttl>] [<class>[^]] <type>[^] <data>
 *        NS ...
 *        AR ...
 *
 *  Inside recv{}:
 *     - The special <name> "%bad" generates a malformed name (c0 00).
 *     - Omitting <ttl> uses last explicit ttl this pkt, or the default ttl
 *     - Omitting QU <class> means "ANY". Elsewhere means "IN".
 *     - The special section "%omit" means to increment the section counter but
 *       generate no data. e.g. "NS %omit"
 *  Inside sent{}:
 *     - Wildcard "*" can be used for <ttl> or <data>
 *     - Omitting <ttl> means "*"
 *     - Omitting <class> means "CLASS*"
 *  Both:
 *     - Starting a <name> with "=" means uncompressed; "\=" means compressed.
 *     - Following <class> or <type> with "^" means +0x8000
 *     - "CLASS<int>" and "TYPE<int>" can be used for <class>, <type>.
 *
 */

#include <assert.h>
#include <ctype.h>
#include <err.h>
#include <errno.h>
#include <ifaddrs.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

#include <sys/socket.h>
#include <sys/types.h>
#include <sys/ioctl.h>

#include <arpa/inet.h>
#include <net/if.h>
#include <netinet/in.h>

#include "respond.h"
#include "verbose.h"
#include "name.h"
#include "zones.h"
#include "rr.h"
#include "table.h"

#define class class_   /* reserved word */

/* Diagnostic output */

#define verbose_puts(m) \
	do { if (verbose) fputs(m, stderr); } while (0)
#define verbose_putc(c) \
	do { if (verbose) putc(c, stderr); } while (0)
#define verbose_printf(...) \
	do { if (verbose) fprintf( stderr, __VA_ARGS__); } while (0)

static const char ERROR[] = "\033[41m ERROR \033[m";
static const char WARNING[] = "\033[31;1mWARNING\033[m";

/* Location within a text input file, used for diag and error messages */
struct loc {
	const char *filename;
	int lineno;
} loc;

static const char *
loc_to_str(const struct loc *loc)
{
	static char buf[1024];

	if (!loc->filename)
		return "(null loc)";

	snprintf(buf, sizeof buf, "%s:%d", loc->filename, loc->lineno);
	return buf;
}

/*------------------------------------------------------------
 * script file: lexical analysis
 */

static struct {
	FILE *f;				/* private to script_nextch()*/
	struct loc loc;				/* location before lookahead */
	int lookahead;				/* single char/EOF lookahead */
} script;

static void
script_open(const char *filename)
{
	FILE *f = fopen(filename, "r");
	if (!f)
		err(1, "%s", filename);
	script.f = f;
	script.loc.filename = filename;
	script.loc.lineno = 1;
	script.lookahead = fgetc(script.f);	/* prime the lookahead */
}

static const char *
script_loc()
{
	return loc_to_str(&script.loc);
}

static void
script_close()
{
	if (script.f) {
		fclose(script.f);
		script.f = NULL;
	}
	script.lookahead = EOF;
}

static int
script_nextch()					/* return lookahead, advance */
{
	int ch = script.lookahead;
	if (ch == '\n')
		script.loc.lineno++;
	script.lookahead = getc(script.f);
	return ch;
}

static void
script_warn(const char *fmt, ...)
{
	va_list ap;
	fprintf(stderr, "%s: ", script_loc());
	va_start(ap, fmt);
	vfprintf(stderr,fmt, ap);
	va_end(ap);
	putc('\n', stderr);
}

__attribute__((noreturn))
static void
script_error(const char *fmt, ...)	/* print an error message, and exit */
{
	va_list ap;
	fprintf(stderr, "%s: ERROR: ", script_loc());
	va_start(ap, fmt);
	vfprintf(stderr,fmt, ap);
	va_end(ap);
	if (script.lookahead == EOF)
		fprintf(stderr, ", near EOF\n");
	else if (script.lookahead == '\n')
		fprintf(stderr, ", near end of line\n");
	else if (isprint(script.lookahead))
		fprintf(stderr, ", near '%c'\n", script.lookahead);
	else
		fprintf(stderr, ", near \\%03o\n", script.lookahead);
	abort();
}

static int
script_can_eat(int ch)				/* consume ch iff it is next */
{
	if (script.lookahead != ch)
		return 0;
	(void)script_nextch();
	return 1;
}

static void
script_skip_space()					/* skip over [ \t]* */
{
	while (script.lookahead == ' ' || script.lookahead == '\t')
		(void)script_nextch();
}

static int
script_at_eol()					/* check if at (#.*)?\n */
{
	return script.lookahead == EOF ||
	       script.lookahead == '\n' ||
	       script.lookahead == '#';
}

static void
script_eol()			     /* consume [\t]*(#.*)?\n or error */
{
	script_skip_space();
	if (script.lookahead == '#')
		while (script.lookahead != '\n' && script.lookahead != EOF)
			(void)script_nextch();
	if (script.lookahead != '\n')
		script_error("expected \\n");
	script_nextch();
}

static int
isword(int ch)					/* tests ch is a word-char */
{
	/* A "word" includes DNS names, IPv6 addresses, '%'-directives,
	 * but excludes whitespace and '#' '{' '}' */
	return isalnum(ch) || ch == '%' || ch == '_' || ch == '.'
			   || ch == ':';
}

/* Consume word */
static const char *
script_word()						/* consume \w+ */
{
	static char buf[1024];
	char *b = buf;

	if (!isword(script.lookahead))
		script_error("expected word");
	while (isword(script.lookahead))
		*b++ = script_nextch();
	*b = '\0';
	return buf;
}

static unsigned int
script_int()					/* consume \d+ as decimal */
{
	unsigned int n = 0;

	script_skip_space();
	if (!isdigit(script.lookahead))
		script_error("expected integer");
	while (isdigit(script.lookahead))
		n = n * 10 + script_nextch() - '0';
	return n;
}

static unsigned int
hexval(char ch)				    /* convert a hex digit to value */
{
	return ch >= 'a' ? ch - 'a' + 10 :
	       ch >= 'A' ? ch - 'A' + 10 :
	                   ch - '0';
}

static void
script_hexdata(unsigned int nbytes, unsigned char *buf)	/* consume \x{2n} */
{
	unsigned char ch;

	while (nbytes--) {
		if (!isxdigit(script.lookahead))
			script_error("expecting hexadecimal");
		ch = hexval(script_nextch()) << 4;
		if (!isxdigit(script.lookahead))
			script_error("expecting hexadecimal");
		ch |= hexval(script_nextch());
		*buf++ = ch;
	}
}

static void
script_copy_line(FILE *out)	/* copy this script line out to a stdio file */
{
	int ch;
	do {
		ch = script_nextch();
		if (ch == EOF)
			script_error("unexpected EOF in zones");
		if (putc(ch, out) == EOF)
			script_error("output error");
	} while (ch != '\n');
}

/* safe strcpy(): dst must be an array */
#define safe_strcpy(dst, src) do {				\
	const char *_src = (src);				\
	unsigned int _sz = strlen(_src) + 1;			\
	if (_sz > sizeof (dst))					\
		script_error("overlong string '%s'", _src);	\
	memcpy(dst, _src, _sz);					\
    } while (0)

static void
print_hexdump(FILE *f, const void *data, unsigned int len, unsigned int offset)
{
	unsigned int i;

	for (i = 0; i < len; i++) {
		if ((i & 15) == 0)
			fprintf(f, "%04x: ", i + offset);
		if ((i & 7) == 0)
			fputc(' ', f);
		fprintf(f, " %02x", ((unsigned char *)data)[i]);
		if ((i & 15) == 15 || i == len - 1)
			fputc('\n', f);
	}
}


/*------------------------------------------------------------
 * mock clock_gettime()
 */

static struct timespec mock_clock;

int
clock_gettime(clockid_t clk_id, struct timespec *res)
{
	*res = mock_clock;
	return 0;
}

static void
mock_clock_advance(unsigned int msec)
{
	long nsec = (msec % 1000) * 1000000;
	long sec = msec / 1000;

	mock_clock.tv_nsec += nsec;
	if (mock_clock.tv_nsec >= 1000000000) {
		mock_clock.tv_nsec -= 1000000000;
		mock_clock.tv_sec++;
	}
	mock_clock.tv_sec += sec;
}

static void
script_process_sleep()				/* sleep <int>[m] */
{
	unsigned int msec;

	msec = script_int();
	script_skip_space();
	if (script.lookahead == 'm')
		(void) script_nextch();
	else
		msec *= 1000;
	script_eol();
	mock_clock_advance(msec);
	verbose_printf("%s: sleep %u m\n", script_loc(), msec);
}

/*------------------------------------------------------------
 * mock gethostname()
 */

static char mock_hostname[255] = "testhost";

int
gethostname(char *name, size_t len)
{
        size_t sz = strlen(mock_hostname) + 1;
	assert(len >= sz);
        memcpy(name, mock_hostname, sz);
        return 0;
}

static void
script_process_hostname()			/* hostname <name> */
{
	const char *hostname = script_word();
	safe_strcpy(mock_hostname, hostname);
	verbose_printf("%s: hostname %s\n", script_loc(), mock_hostname);
	script_eol();
}

/*------------------------------------------------------------
 * mock getifaddrs()
 */

#define MAX_IFADDRS 16
static struct sockaddr_storage mock_ifaddr[MAX_IFADDRS];
static unsigned int mock_ifaddr_count;

int
getifaddrs(struct ifaddrs **ifap)
{
	unsigned int i;
	struct ifaddrs **ifap_orig = ifap;
	static char ifname[] = "eth0";

	for (i = 0; i < mock_ifaddr_count; i++) {
		struct ifaddrs *ifa = calloc(1, sizeof *ifa);
		if (!ifa) {
			*ifap = NULL;
			freeifaddrs(*ifap_orig);
			return -1;
		}
		ifa->ifa_name = ifname;
		ifa->ifa_flags = 0
			| IFF_UP
			| IFF_RUNNING
			| IFF_MULTICAST
			;
		ifa->ifa_addr = (struct sockaddr *)&mock_ifaddr[i];
		*ifap = ifa;
		ifap = &ifa->ifa_next;
	}
	*ifap = NULL;
	return 0;
}

void
freeifaddrs(struct ifaddrs *ifa)
{
	struct ifaddrs *ifa_next;

	while (ifa) {
		ifa_next = ifa->ifa_next;
		free(ifa);
		ifa = ifa_next;
	}
}

static void
script_process_ifaddr()			/* ifaddr [clear|<ip4>|<ip6>] */
{
	char buf[INET6_ADDRSTRLEN];
	const char *word = script_word();

	if (strcmp(word, "clear") == 0) {
		mock_ifaddr_count = 0;
		memset(mock_ifaddr, 0, sizeof mock_ifaddr);
		verbose_printf("%s: ifaddr clear\n", script_loc());
	} else if (strchr(word, ':')) {
		struct sockaddr_in6 *sin6 =
		    (struct sockaddr_in6 *)&mock_ifaddr[mock_ifaddr_count++];
		assert(mock_ifaddr_count < MAX_IFADDRS);
		sin6->sin6_family = AF_INET6;
		if (inet_pton(AF_INET6, word, &sin6->sin6_addr) != 1)
			script_error("bad IPv6 address '%s'", word);
		verbose_printf("%s: ifaddr %s\n", script_loc(),
			inet_ntop(AF_INET6, &sin6->sin6_addr, buf, sizeof buf));
	} else {
		struct sockaddr_in *sin =
		    (struct sockaddr_in *)&mock_ifaddr[mock_ifaddr_count++];
		assert(mock_ifaddr_count < MAX_IFADDRS);
		sin->sin_family = AF_INET;
		if (inet_pton(AF_INET, word, &sin->sin_addr) != 1)
			script_error("bad IPv4 address '%s'", word);
		verbose_printf("%s: ifaddr %s\n", script_loc(),
			inet_ntop(AF_INET, &sin->sin_addr, buf, sizeof buf));
	}
	script_eol();
}

/*------------------------------------------------------------
 * "zones" directive
 */

static void
script_process_zones()				/* zones { \n <line>* \n } */
{
	const char *word;

	if (script_can_eat('{')) {
		FILE *f;
		char filename[256];

		verbose_printf("%s: zones { ... }\n", script_loc());
		script_eol();
		snprintf(filename, sizeof filename,
			"/tmp/respond.tmp.%d.db", getpid());
		f = fopen(filename, "w");
		if (!f)
			script_error("%s: %s", filename, strerror(errno));
		while (script.lookahead != EOF && !script_can_eat('}')) {
			script_can_eat('\t');
			script_copy_line(f);
		}
		fclose(f);
		script_eol();
		if (zones_load(filename) == -1)
			script_error("bad zones");
		if (unlink(filename) == -1)
			warn("%s", filename);
		verbose_printf("%s: # end of zones\n", script_loc());
		return;
	}

	word = script_word();
	if (strcmp(word, "clear") == 0) {		/* zones clear */
		verbose_printf("%s: zones clear\n", script_loc());
		script_eol();
		zones_clear();
		return;
	}

	script_error("expected {...} or 'clear', but got '%s'", word);
}

/*------------------------------------------------------------
 * network-endian conversion functions
 */

static unsigned int
put32(uint32_t val, char *p)
{
	p[0] = (val >> 24) & 0xff;
	p[1] = (val >> 16) & 0xff;
	p[2] = (val >>  8) & 0xff;
	p[3] = (val >>  0) & 0xff;
	return 4;
}

static unsigned int
put16(uint16_t val, char *p)
{
	p[0] = (val >>  8) & 0xff;
	p[1] = (val >>  0) & 0xff;
	return 2;
}

static uint16_t
get16(const char *p)
{
	return (p[0] & 0xff) << 8 |
	       (p[1] & 0xff) << 0;
}

static uint32_t
get32(const char *p)
{
	return (p[0] & 0xff) << 24 |
	       (p[1] & 0xff) << 16 |
	       (p[2] & 0xff) <<  8 |
	       (p[3] & 0xff) <<  0;
}

/*------------------------------------------------------------
 * specified/unspecified/wildcard monad
 */

enum spec {
	SPEC_UNSPECIFIED = 0,
	SPEC_SPECIFIED,
	SPEC_WILDCARD
};

#define DECL_SPEC(T)		struct { enum spec spec; T value; }
#define SPEC_SET_UNSPECIFIED(s)	do { (s).spec = SPEC_UNSPECIFIED; } while (0)
#define SPEC_SET_WILDCARD(s)	do { (s).spec = SPEC_WILDCARD; } while (0)
#define SPEC_SET_SPECIFIED(s)	do { (s).spec = SPEC_SPECIFIED; } while (0)
#define SPEC_IS_SPECIFIED(s)	((s).spec == SPEC_SPECIFIED)
#define SPEC_IS_WILDCARD(s)	((s).spec == SPEC_WILDCARD)
#define SPEC_IS_UNSPECIFIED(s)	((s).spec == SPEC_UNSPECIFIED)
#define SPEC_VALUE(s)		(s).value

/* Returns s.value, or a default value when s is UNSPECIFIED */
#define SPEC_GET(s, default_value) \
	(SPEC_IS_SPECIFIED(s) ? SPEC_VALUE(s) : default_value)

/*------------------------------------------------------------
 * A domain name encoded within a packet, possibly uncompressed
 */

struct pkt_name {
	DECL_SPEC(unsigned char) compress;
	_DN char name[NAME_MAX];
};

static uint16_t
pkt_name_pack(char *pkt, uint16_t pktsz, uint16_t offset,
	uint16_t *offsets, const struct pkt_name *name)
{
	uint16_t sz;

	if (SPEC_GET(name->compress, 1))
		return name_pack(pkt, pktsz, offset,
			name->name, offsets);
	sz = name_size(name->name);
	assert(offset + sz <= pktsz);
	memcpy(pkt + offset, name->name, sz);
	return offset + sz;
}

static uint16_t
pkt_name_unpack(const char *pkt, uint16_t pktlen, uint16_t offset,
	struct pkt_name *n)
{
	uint16_t after;

	after = name_unpack(pkt, pktlen, offset, n->name);
	if (!after)
		return 0;
	SPEC_VALUE(n->compress) =
		after != (offset + name_size(n->name));
	SPEC_SET_SPECIFIED(n->compress);
	return after;
}

static int
pkt_name_is_eq(const struct pkt_name *a, const struct pkt_name *b)
{
	if (SPEC_IS_SPECIFIED(a->compress) &&
	    SPEC_IS_SPECIFIED(b->compress) &&
            SPEC_VALUE(a->compress) != SPEC_VALUE(b->compress))
		return 0;
	return name_eq(a->name, b->name);
}

static const char *
pkt_name_to_str(const struct pkt_name *n)
{
	static char buf[260];

	snprintf(buf, sizeof buf, "%s%s",
		SPEC_IS_SPECIFIED(n->compress) &&
		!SPEC_VALUE(n->compress) ? "=" : "",
		name_repr(n->name));
	return buf;
}

static void
pkt_name_print(FILE *f, const struct pkt_name *name)
{
	fputs(pkt_name_to_str(name), f);
}

/*------------------------------------------------------------
 * structured RDATA wihin a packet
 */

/* RDATAs can be described by a format string, keyed by resource type */
static const struct table rdata_format[] = {
        { T_A,          "a" },
        { T_AAAA,       "A" },
        { T_SRV,        "hhhn" },
        { T_MX,         "hn" },
        { T_PTR,        "n" },
        { T_CNAME,      "n" },
        { T_NS,         "n" },
        { T_TXT,        "t+" },
        { T_HINFO,      "tt" },
        {}
};

/* model of an RDATA sequence within a DNS packet */
struct pkt_rdata {
	unsigned int len;
#define MAX_RDATA_MEMBERS 5
	char form[MAX_RDATA_MEMBERS];
	union pkt_rdata_member {
		unsigned char a[4];			/* in_addr_t */
		unsigned char A[16];			/* in6_addr_t */
		uint16_t h;
		struct pkt_txt {
			uint8_t len;
			unsigned char data[255];	/* UTF-8 */
		} t;
		struct pkt_name n;
		struct pkt_hex {
			uint16_t len;
			unsigned char *data;
		} x;
	} *member[MAX_RDATA_MEMBERS];
};

static void
pkt_rdata_init(struct pkt_rdata *rd)
{
	rd->len = 0;
};

static void
pkt_rdata_fini(struct pkt_rdata *rd)
{
	unsigned int i;

	/* Release the members added by pkt_rdata_member_new() */
	for (i = 0; i < rd->len; i++) {
		union pkt_rdata_member *m = rd->member[i];
		switch (rd->form[i]) {
		case 'x': free(m->x.data); break;
		}
		free(m);
	}
	rd->len = 0;
}

/* construct and append a typed member to an RDATA model */
static union pkt_rdata_member *
pkt_rdata_member_new(struct pkt_rdata *rd, char form)
{
	union pkt_rdata_member *m;
	assert(rd->len < MAX_RDATA_MEMBERS);
	switch (form) {
	case 'a': m = calloc(1, sizeof m->a); break;
	case 'A': m = calloc(1, sizeof m->A); break;
	case 'h': m = calloc(1, sizeof m->h); break;
	case 't': m = calloc(1, sizeof m->t); break;
	case 'n': m = calloc(1, sizeof m->n); break;
	case 'x': m = calloc(1, sizeof m->x); break;
	default: errx(1, "unknown form '%c'", form);
	}
	if (!m) err(1, "malloc");
	rd->form[rd->len] = form;
	rd->member[rd->len] = m;
	rd->len++;
	return m;
}

static const char *
pkt_rdata_member_to_str(char form, const union pkt_rdata_member *m)
{
	static char buf[1 + 255 * 4 + 1 + 1];
	char *b = buf;
	char * const b_end = &buf[sizeof buf];
	unsigned int i;
	unsigned char c;

	switch (form) {
	case 'a':
		inet_ntop(AF_INET, m->a, buf, sizeof buf);
		return buf;
	case 'A':
		inet_ntop(AF_INET6, m->A, buf, sizeof buf);
		return buf;
	case 'h':
		snprintf(buf, sizeof buf, "%" PRIu16, m->h);
		return buf;
	case 't':
		*b ++ = '"';
		for (i = 0; i < m->t.len; i++) {
			c = m->t.data[i];
			if (c < ' ' || c == 0x7f)
				b += snprintf(b, b_end - b, "\\%03u", c);
			else {
				if (c == '\\' || c == '"')
					*b++ = '\\';
				*b++ = '\\';
			}
		}
		*b++ = '"';
		*b = '\0';
		return buf;
	case 'n':
		return pkt_name_to_str(&m->n);
	case 'x':
		b += snprintf(b, b_end - b, "\\# %" PRIu16, m->x.len);
		if (m->x.len)
			*b++ = ' ';
		for (i = 0; i < m->x.len; i++)
			b += snprintf(b, b_end - b, "%02x",
				m->x.data[i] & 0xff);
		*b = '\0';
		return buf;
	default:
		errx(1, "unknown form '%c'", form);
	}
}

static void
pkt_rdata_member_print(FILE *f, char form, const union pkt_rdata_member *m)
{
	fputs(pkt_rdata_member_to_str(form, m), f);
}

static uint16_t
pkt_rdata_member_pack(char *pkt, uint16_t pktsz, uint16_t offset,
	uint16_t *offsets, char form, const union pkt_rdata_member *m)
{
	switch (form) {
	case 'a':
		assert(offset + sizeof m->a <= pktsz);
		memcpy(pkt + offset, m->a, sizeof m->a);
		return offset + sizeof m->a;
	case 'A':
		assert(offset + sizeof m->A <= pktsz);
		memcpy(pkt + offset, m->A, sizeof m->A);
		return offset + sizeof m->A;
	case 'h':
		assert(offset + sizeof (uint16_t) <= pktsz);
		put16(m->h, pkt + offset);
		return offset + sizeof (uint16_t);
	case 't':
		assert(offset + 1 + m->t.len <= pktsz);
		pkt[offset] = m->t.len;
		memcpy(pkt + offset + 1, m->t.data, m->t.len);
		return offset + 1 + m->t.len;
	case 'n':
		return pkt_name_pack(pkt, pktsz, offset,
			offsets, &m->n);
	case 'x':
		assert(offset + m->x.len <= pktsz);
		memcpy(pkt + offset, m->x.data, m->x.len);
		return offset + m->x.len;
	default:
		errx(1, "unknown form '%c'", form);
	}
}

static uint16_t
pkt_rdata_member_unpack(const char *pkt, uint16_t pktlen, uint16_t offset,
	char form, union pkt_rdata_member *m)
{
	uint8_t len;

	switch (form) {
	case 'a':
		if (offset + sizeof m->a > pktlen) return 0;
		memcpy(m->a, pkt + offset, sizeof m->a);
		return offset + sizeof m->a;
	case 'A':
		if (offset + sizeof m->A > pktlen) return 0;
		memcpy(m->A, pkt + offset, sizeof m->A);
		return offset + sizeof m->a;
	case 'h':
		if (offset + sizeof (uint16_t) > pktlen) return 0;
		m->h = get16(pkt + offset);
		return offset + sizeof (uint16_t);
	case 't':
		if (offset + 1 > pktlen) return 0;
		len = pkt[offset];
		if (offset + 1 + len > pktlen) return 0;
		m->t.len = len;
		memcpy(m->t.data, pkt + offset + 1, len);
		return offset + 1 + len;
	case 'n':
		return pkt_name_unpack(pkt, pktlen, offset, &m->n);
	case 'x':
		free(m->x.data);
		len = pktlen - offset;
		m->x.len = len;
		if (!len)
			m->x.data = NULL;
		else {
			m->x.data = malloc(len);
			if (!m->x.data) err(1, "malloc");
		}
		memcpy(m->x.data, pkt + offset, len);
		return pktlen;
	default:
		errx(1, "unknown form '%c'", form);
	}
}

static int
pkt_rdata_member_is_eq(char form, const union pkt_rdata_member *a,
	const union pkt_rdata_member *b)
{
	uint16_t len;
	const void *ap, *bp;

	switch (form) {
	case 'a': len = sizeof a->a; ap = &a->a; bp = &b->a; break;
	case 'A': len = sizeof a->A; ap = &a->A; bp = &b->A; break;
	case 'h': len = sizeof a->h; ap = &a->h; bp = &b->h; break;
	case 't': if (a->t.len != b->t.len) return 0;
		  len = a->t.len; ap = &a->t.data; bp = &b->t.data; break;
	case 'n': return pkt_name_is_eq(&a->n, &b->n);
	case 'x': if (a->x.len != b->x.len) return 0;
		  len = a->x.len; ap = &a->x.data; bp = &b->x.data; break;
	default: errx(1, "unknown form '%c'", form);
	}
	return memcmp(ap, bp, len) == 0;
}

/* decls to keep the pkt_rdata_member fns together */
static void script_pkt_txt(struct pkt_txt *txt);
static void script_pkt_name(struct pkt_name *name);
static void script_pkt_hex(struct pkt_hex *m);

static void
script_pkt_rdata_member(char form, union pkt_rdata_member *m)
{
	const char *word;

	switch (form) {
	case 'a':
		if (inet_pton(AF_INET, word=script_word(), m->a) != 1)
			script_error("bad IPv4 address '%s'", word);
		break;
	case 'A':
		if (inet_pton(AF_INET6, word=script_word(), m->A) != 1)
			script_error("bad IPv6 address '%s'", word);
		break;
	case 'h':
		m->h = script_int();
		break;
	case 't':
		script_pkt_txt(&m->t);
		break;
	case 'n':
		script_pkt_name(&m->n);
		break;
	case 'x':
		script_pkt_hex(&m->x);
		break;
	default:
		errx(1, "unknown form '%c'", form);
	}
}

/*------------------------------------------------------------
 * recv{}/sent{} packet descriptor (struct pkt)
 */

static const struct table opcode_names[] = {
	{ 0, "QUERY" },
	{ 1, "IQUERY" },
	{ 2, "STATUS" },
	{}
};

static const struct table rcode_names[] = {
	{ 0, "NOERROR" },
	{ 1, "FORMERR" },
	{ 2, "SERVFAIL" },
	{ 3, "NXDOMAIN" },
	{ 4, "NOTIMP" },
	{ 5, "REFUSED" },
	{}
};

/* The flag bits within octets 2 and 3 of the DNS header */
static const struct flag_desc {
	const char *name;
	unsigned char shift;
	unsigned char mask;
	const struct table *codenames;
} pkt_flag_descs[] = {
	{ "qr",     15, 0x1 },
	{ "opcode", 11, 0xf, opcode_names },
	{ "aa",     10, 0x1 },
	{ "tc",      9, 0x1 },
	{ "rd",      8, 0x1 },
	{ "ra",      7, 0x1 },
	{ "rcode",   0, 0xf, rcode_names },
#define NFLAGS 7
	{}
};

/* size of a DNS packet header when encoded */
#define DNS_HEADER_LEN			12
#define DNS_HEADER_OFFSET_ID		 0
#define DNS_HEADER_OFFSET_FLAGS		 2
#define DNS_HEADER_OFFSET_COUNT(sect)	(4 + 2 * (sect))

/* DNS packet descriptor */
struct pkt {
	enum pkt_context {
		CONTEXT_INVALID, CONTEXT_RECV, CONTEXT_SENT
	} context;
	struct loc loc;

	DECL_SPEC(uint16_t) id;
	struct loc id_loc;

	DECL_SPEC(uint16_t) flag[NFLAGS]; /* one for each pkt_flag_descs[] */
	struct loc flag_loc;

#define SECTION_QU	0	/* Question section */
#define SECTION_AN	1	/* Answer section */
#define SECTION_NS	2	/* Authoritative (NS) section */
#define SECTION_AR	3	/* Additional records section */
#define NSECTIONS	4
	uint16_t count[NSECTIONS];

	unsigned int npart; /* == sum(count[]) */
	struct pkt_part {
		struct loc loc;
		struct pkt_name name;
		DECL_SPEC(uint16_t) class;
		DECL_SPEC(uint16_t) type;
		DECL_SPEC(uint32_t) ttl;		/* invalid for QU */
		DECL_SPEC(struct pkt_rdata) rdata;	/* invalid for QU */
		unsigned char section;
#define MAX_PARTS 32
	} part[MAX_PARTS];

	struct query_info qinfo;
};

static uint32_t default_ttl = 3600;

static const char *section_name[NSECTIONS] = {
	[SECTION_QU] = "QU",
	[SECTION_AN] = "AN",
	[SECTION_NS] = "NS",
	[SECTION_AR] = "AR",
};

static void
pkt_init(struct pkt *pkt, enum pkt_context context)
{
	memset(pkt, 0, sizeof *pkt);
	pkt->context = context;
	pkt->qinfo.time = mock_clock;
}

static void
pkt_fini(struct pkt *pkt)
{
	unsigned int p;

	for (p = 0; p < pkt->npart; p++)
		if (SPEC_IS_SPECIFIED(pkt->part[p].rdata))
			pkt_rdata_fini(&SPEC_VALUE(pkt->part[p].rdata));
}

static void
pkt_print(FILE *f, const struct pkt *pkt)
{
	unsigned int fi;
	unsigned int p;
	unsigned char all_flags_are_unspecified;

	if (!pkt || pkt->context == CONTEXT_INVALID) {
		fputs("nothing", f);
		return;
	}

	fputs("{\n", f);

	if (SPEC_IS_SPECIFIED(pkt->id))
		fprintf(f, "\tid %" PRIu32 "\n", SPEC_VALUE(pkt->id));
	else if (SPEC_IS_WILDCARD(pkt->id))
		fputs("\tid *\n", f);

	all_flags_are_unspecified = 1;
	for (fi = 0; fi < NFLAGS; fi++) {
		if (!SPEC_IS_UNSPECIFIED(pkt->flag[fi])) {
			all_flags_are_unspecified = 0;
			break;
		}
	}
	if (!all_flags_are_unspecified) {
		fputs("\tflags", f);
		for (fi = 0; fi < NFLAGS; fi++) {
			const char *codename;
			const struct flag_desc *desc = &pkt_flag_descs[fi];

			if (SPEC_IS_UNSPECIFIED(pkt->flag[fi]))
				continue;
			if (SPEC_IS_WILDCARD(pkt->flag[fi])) {
				fprintf(f, " %s=*", desc->name);
				continue;
			}
			if (desc->codenames &&
			    (codename = table_to_str(desc->codenames,
						SPEC_VALUE(pkt->flag[fi]))))
				fprintf(f, " %s", codename);
			else
				fprintf(f, " %s=%" PRIu16, desc->name,
					SPEC_VALUE(pkt->flag[fi]));
		}
		putc('\n', f);
	}

	for (p = 0; p < pkt->npart; p++) {
		const struct pkt_part *part = &pkt->part[p];
		const char *s;

		fprintf(f, "\t%s ", section_name[part->section]);
		pkt_name_print(f, &part->name);

		if (part->section != SECTION_QU) {
			if (SPEC_IS_WILDCARD(part->ttl))
				fputs(" ttl=*", f);
			else if (SPEC_IS_SPECIFIED(part->ttl))
				fprintf(f, " %" PRIu32,
					SPEC_VALUE(part->ttl));
		}

		if (SPEC_IS_WILDCARD(part->class))
			fputs(" CLASS*", f);
		else if (!SPEC_IS_SPECIFIED(part->class))
			; /* nothing */
		else if (SPEC_VALUE(part->class) == C_ANY)
			fputs(" C_ANY", f);
		else if ((s = table_to_str(rr_class_table,
				SPEC_VALUE(part->class))))
			fprintf(f, " %s", s);
		else
			fprintf(f, " CLASS%" PRIu16, SPEC_VALUE(part->class));

		if (SPEC_IS_WILDCARD(part->type))
			fputs(" TYPE*", f);
		else if (!SPEC_IS_SPECIFIED(part->type))
			; /* nothing */
		else if (SPEC_VALUE(part->type) == T_ANY)
			fputs(" ANY", f);
		else if ((s = table_to_str(rr_type_table,
				SPEC_VALUE(part->type))))
			fprintf(f, " %s", s);
		else
			fprintf(f, " TYPE%" PRIu16, SPEC_VALUE(part->type));

		if (part->section == SECTION_QU)
			; /* nothing */
		else if (SPEC_IS_WILDCARD(part->rdata))
			fputs(" *", f);
		else if (SPEC_IS_SPECIFIED(part->rdata)) {
			unsigned int i;
			const struct pkt_rdata *rd = &SPEC_VALUE(part->rdata);
			for (i = 0; i < rd->len; i++) {
				fputc(' ', f);
				pkt_rdata_member_print(f, rd->form[i],
					rd->member[i]);
			}
		}

		fprintf(f, " ;");
		if (part->loc.filename)
			fprintf(f, " [%s]", loc_to_str(&part->loc));
		if (SPEC_IS_SPECIFIED(part->rdata))
			fprintf(f, " fmt='%.*s'",
				SPEC_VALUE(part->rdata).len,
				SPEC_VALUE(part->rdata).form);
		putc('\n', f);
	}
	putc('}', f);
}

static struct pkt_part *
pkt_part_new(struct pkt *pkt, unsigned char section)
{
	struct pkt_part *part;
	unsigned int prev_section =
	    pkt->npart ? pkt->part[pkt->npart - 1].section : SECTION_QU;

	/* Parts must occupy increasing sections */
	if (section < prev_section)
		script_error("reversed part order");

	assert(pkt->npart < MAX_PARTS);
	part = &pkt->part[pkt->npart++];
	part->section = section;
	pkt->count[section]++;
	return part;
}

static void
script_part_class_type(struct pkt_part *part)	/* [[^]<class>] [^]<type> */
{
	const char *word;
	uint16_t caret;

	/* class */
	word = script_word();
	caret = script_can_eat('^') ? 0x8000 : 0;
	script_skip_space();

	if (table_to_val16(rr_class_table, word, &SPEC_VALUE(part->class)) ||
	    sscanf(word, "CLASS%" SCNu16, &SPEC_VALUE(part->class))) {
		SPEC_VALUE(part->class) |= caret;
		SPEC_SET_SPECIFIED(part->class);
	} else if (strcmp(word, "ANY") == 0) {
		SPEC_VALUE(part->class) = C_ANY | caret;
		SPEC_SET_SPECIFIED(part->class);
	} else if (strcmp(word, "*") == 0) {
		if (caret) script_error("unexpected ^");
		SPEC_SET_WILDCARD(part->class);
	}

	if (!SPEC_IS_UNSPECIFIED(part->class)) {
		word = script_word();
		caret = script_can_eat('^') ? 0x8000 : 0;
		script_skip_space();
	}

	/* type */
	if (table_to_val16(rr_type_table, word, &SPEC_VALUE(part->type)) ||
	     sscanf(word, "TYPE%" SCNu16, &SPEC_VALUE(part->type))) {
		SPEC_VALUE(part->type) |= caret;
		SPEC_SET_SPECIFIED(part->type);
	} else if (strcmp(word, "ANY") == 0) {
		SPEC_VALUE(part->type) = T_ANY | caret;
		SPEC_SET_SPECIFIED(part->type);
	} else if (strcmp(word, "*") == 0) {
		if (caret) script_error("unexpected ^");
		SPEC_SET_WILDCARD(part->type);
	} else
		script_error("unknown type '%s'", word);
}

static void
script_pkt_process_flag(struct pkt *pkt)	/* [!]<flag>[=<value>] */
{
	const struct flag_desc *f;
	const char *word;
	unsigned flag;
	int wildcard = 0;
	unsigned int value = 0;
	int bang;

	/* process a single flag assertion of
	 *   !<flag>           implies =0 for single-bit flags
	 *   <flag>            implies =1 for single-bit flags
	 *   <flag>=*
	 *   <flag>=<int>
	 *   <codename>        implies <flag>=<value> from codename lookup
	 */

	bang = script_can_eat('!');
	word = script_word();
	script_skip_space();
	for (f = pkt_flag_descs, flag = 0; f->name; f++, flag++) {
		if (strcmp(word, f->name) == 0) {
			if (bang) {
				if (f->mask != 1)
				    script_error("!%s: not boolean", word);
				value = 0;
			} else if (script_can_eat('=')) {
				script_skip_space();
				if (pkt->context == CONTEXT_SENT &&
				    script_can_eat('*'))
					wildcard = 1;
				else
					value = script_int();
			} else if (f->mask == 1)
				value = 1;
			else
				script_error("expected '=' after '%s'", word);
			break;
		} else if (f->codenames) {
			const struct table *cn;
			for (cn = f->codenames; cn->str; cn++) {
				if (strcmp(word, cn->str) != 0)
					continue;
				if (bang)
					script_error("!%s: invalid", word);
				if (script.lookahead == '=')
					script_error("unexpected '='");
				break;
			}
			if (cn->str) {
				value = cn->value;
				break;
			}
		}
	}
	if (!f->name)
		script_error("unknown flag '%s'", word);
	assert(flag < NFLAGS);
	if (wildcard) {
		SPEC_SET_WILDCARD(pkt->flag[flag]);
	} else {
		if (value & ~f->mask)
			script_error("value %u too large for flag %s",
				value, f->name);
		SPEC_SET_SPECIFIED(pkt->flag[flag]);
		SPEC_VALUE(pkt->flag[flag]) = value;
	}
}

static void
script_pkt_txt(struct pkt_txt *txt)		/* "(\<ddd>|\<ch>|<ch>)*" */
{
	uint8_t len = 0;
	unsigned char ch;

	if (!script_can_eat('"'))
		script_error("expected '\"'");
	while (!script_can_eat('"')) {
		if (script.lookahead == '\n' || script.lookahead == EOF)
			script_error("unterminated string");
		if (script_can_eat('\\') && isdigit(script.lookahead)) {
			ch = (script_nextch() - '0') * 100;
			if (!isdigit(script.lookahead))
				script_error("expected digit");
			ch += (script_nextch() - '0') * 10;
			if (!isdigit(script.lookahead))
				script_error("expected digit");
			ch += (script_nextch() - '0');
		} else
			ch = script_nextch();
		txt->data[len++] = ch;
	}
	txt->len = len;
}

static void
script_pkt_hex(struct pkt_hex *hex)			/* \# <int> <hex> */
{
	unsigned int len;

	if (!script_can_eat('\\') || !script_can_eat('#'))
		script_error("expected '\\#'");
	script_skip_space();
	len = script_int();
	if (len > 0xffff)
		script_error("%u, too much data", len);
	hex->len = len;
	if (!len)
		return;
	free(hex->data);
	hex->data = malloc(len);
	if (!hex->data)
		err(1, "malloc");
	script_hexdata(len, hex->data);
	script_skip_space();
}

static void
script_pkt_name(struct pkt_name *name)			/* [=]<dotted-name> */
{
	const char *word;

	script_skip_space();
	if (script_can_eat('=')) {
		SPEC_VALUE(name->compress) = 0;
		SPEC_SET_SPECIFIED(name->compress);
		script_skip_space();
	} else if (script_can_eat('\\')) {
		if (!script_can_eat('='))
			script_error("expected '='");
		SPEC_VALUE(name->compress) = 1;
		SPEC_SET_SPECIFIED(name->compress);
		script_skip_space();
	} else
		SPEC_SET_UNSPECIFIED(name->compress);

	word = script_word();
	if (name_store(name->name, "", word) == -1)
		script_error("name '%s': %s", word, name_error);
	script_skip_space();
}

static void
script_part_name(struct pkt_part *part)
{
	script_pkt_name(&part->name);
}

static void
script_part_data(struct pkt_part *part)		/* <member>s or '*' or \#... */
{
	uint16_t type;
	struct pkt_rdata *rdata;
	const char *fmt;
	char form;
	union pkt_rdata_member *m;

	assert(SPEC_IS_SPECIFIED(part->type));
	type = SPEC_VALUE(part->type) & ~0x8000;

	/* The entire RDATA can be wildcarded */
	if (script_can_eat('*')) {
		SPEC_SET_WILDCARD(part->rdata);
		script_skip_space();
		return;
	}

	fmt = table_to_str(rdata_format, type);
	if (!fmt) {
		if (script.lookahead != '\\')
			script_error("unknown type %" PRIu16, type);
		fmt = "x";
	}

	/* Build up a pkt_rdata based on the format string */
	rdata = &SPEC_VALUE(part->rdata);
	pkt_rdata_init(rdata);
	while (*fmt) {
		if (*fmt == '+') {
			/* '+' repeats previous format until eol */
			if (script_at_eol())
				break;
			form = fmt[-1];
		} else
			form = *fmt++;

		m = pkt_rdata_member_new(rdata, form);
		script_pkt_rdata_member(form, m);
		script_skip_space();
	}

	SPEC_SET_SPECIFIED(part->rdata);
}

static void
script_process_qu_part(struct pkt *pkt)
{
	struct pkt_part *part = pkt_part_new(pkt, SECTION_QU);

	part->loc = script.loc;
	script_part_name(part);
	script_part_class_type(part);
}

static void
script_process_part(struct pkt *pkt, unsigned char section)
{
	struct pkt_part *part = pkt_part_new(pkt, section);

	part->loc = script.loc;
	script_part_name(part);
	if (isdigit(script.lookahead)) {
		SPEC_VALUE(part->ttl) = script_int();
		SPEC_SET_SPECIFIED(part->ttl);
		script_skip_space();
	}
	script_part_class_type(part);

	if (SPEC_IS_SPECIFIED(part->type))
		script_part_data(part);
}

static void
script_pkt_process_line(struct pkt *pkt)
{
	const char *word;

	script_skip_space();
	if (script.lookahead == '}')
		return;
	if (script_at_eol()) {
		script_eol();
		return;
	}

	word = script_word();
	script_skip_space();
	if (strcmp(word, "id") == 0) {
		pkt->id_loc = script.loc;
		if (pkt->context == CONTEXT_SENT && script_can_eat('*'))
			SPEC_SET_WILDCARD(pkt->id);
		else {
			SPEC_VALUE(pkt->id) = script_int();
			SPEC_SET_SPECIFIED(pkt->id);
		}
	} else if (strcmp(word, "flags") == 0) {
		pkt->flag_loc = script.loc;
		while (!script_at_eol()) {
			script_pkt_process_flag(pkt);
			script_skip_space();
		}
	} else if (strcmp(word, "QU") == 0) {
		script_process_qu_part(pkt);
	} else if (strcmp(word, "AN") == 0) {
		script_process_part(pkt, SECTION_AN);
	} else if (strcmp(word, "NS") == 0) {
		script_process_part(pkt, SECTION_NS);
	} else if (strcmp(word, "AR") == 0) {
		script_process_part(pkt, SECTION_AR);
	} else {
		script_error("unknown packet directive '%s'", word);
	}
	script_eol();
}

/*------------------------------------------------------------
 * recv
 */

static struct pkt recv_pkt;  /* contains last recv{} pkt desc */
static struct pkt sent_pkt;  /* contains last sent{} pkt desc */

static char actual_sent_data[9000]; /* result of last mdns_respond() */
static int actual_sent_len;

static struct pkt actual_sent_pkt; /* packed&unpacked form of sent_pkt */

static void
clear_actual_sent()
{
	actual_sent_pkt.context = CONTEXT_INVALID;
	actual_sent_len = -1;
}

/* Packs DNS packet data into buf[] based on the
 * description stored in *pkt.
 * Returns number of bytes stored in buf. */
static unsigned int
pkt_to_buf(const struct pkt *pkt, char *buf, unsigned int bufsz)
{
	char *b;
	char *buf_end = buf + bufsz;
	uint16_t flag_word;
	unsigned int i;
	unsigned int p;
	uint16_t offsets[NAME_PACK_OFFSETS] = {0};
	const char *s;
	uint16_t after;

	verbose_puts("pkt_to_buf: {\n");

	assert(bufsz >= DNS_HEADER_LEN);

	/* id */
	verbose_printf("  id 0x%04x\n", SPEC_GET(pkt->id, 0));
	put16(SPEC_GET(pkt->id, 0), buf + DNS_HEADER_OFFSET_ID);

	/* flags and opcodes */
	verbose_puts("  flags");
	flag_word = 0;
	for (i = 0; i < NFLAGS; i++) {
		const struct flag_desc *desc = &pkt_flag_descs[i];
		uint16_t flag_val = SPEC_GET(pkt->flag[i], 0);
		flag_word |= (flag_val & desc->mask) << desc->shift;
		if (verbose && (desc->mask > 1 || flag_val)) {
			verbose_printf(" %s", desc->name);
			if (desc->codenames &&
			    (s = table_to_str(desc->codenames, flag_val)))
				verbose_printf("=%s", s);
			else if (desc->mask > 1)
				verbose_printf("=%x", flag_val);
		}
	}
	put16(flag_word, buf + DNS_HEADER_OFFSET_FLAGS);
	verbose_putc('\n');

	/* section counts */
	for (i = 0; i < NSECTIONS; i++)
		put16(pkt->count[i], buf + DNS_HEADER_OFFSET_COUNT(i));

	b = buf + DNS_HEADER_LEN;
	for (p = 0; p < pkt->npart; p++) {
		const struct pkt_part *part = &pkt->part[p];
		uint16_t type, class;

		assert(b <= buf_end);
		verbose_printf("  %s ", section_name[part->section]);

		/* name */
		after = pkt_name_pack(buf, bufsz, b - buf,
			offsets, &part->name);
		if (!after)
			script_error("packet overflow");
		b = buf + after;
		if (verbose)
			pkt_name_print(stderr, &part->name);

		/* type */
		type = SPEC_GET(part->type, T_ANY);
		b += put16(type, b);

		/* class */
		class = SPEC_GET(part->class, C_ANY);
		b += put16(class, b);

		if (verbose) {
			/* print class */
			if (class == C_ANY)
				verbose_puts(" ANY");
			else if ((s = table_to_str(rr_class_table, class)))
				verbose_printf(" %s", s);
			else
				verbose_printf(" CLASS%" PRIu16, class);

			/* print type */
			if (type == T_ANY)
				verbose_puts(" ANY");
			else if ((s = table_to_str(rr_type_table, type)))
				verbose_printf(" %s", s);
			else
				verbose_printf(" TYPE%" PRIu16, type);
		}

		/* Question sections have no more data */
		if (part->section == SECTION_QU) {
			verbose_putc('\n');
			continue;
		}

		/* ttl */
		b += put32(SPEC_GET(part->ttl, default_ttl), b);

		/* rlength, rdata */
		if (SPEC_IS_SPECIFIED(part->rdata)) {
			unsigned int i;
			const struct pkt_rdata *rd = &SPEC_VALUE(part->rdata);
			for (i = 0; i < rd->len; i++) {
				const union pkt_rdata_member *m =
							    rd->member[i];
				char form = rd->form[i];

				after = pkt_rdata_member_pack(buf, bufsz,
					b - buf, offsets, form, m);
				if (!after)
					script_error("packet overflow");
				b = buf + after;

				if (verbose) {
					verbose_putc(' ');
					pkt_rdata_member_print(stderr,
						form, m);
				}
			}
		} else {
			/* When unspecified, default to \# 0 */
			b += put16(0, b);
			verbose_puts(" \\# 0");
		}
		verbose_putc('\n');
	}
	assert(b <= buf_end);
	verbose_puts("} # end pkt_to_buf\n");
	return b - buf;
}

static uint16_t
buf_to_pkt_part(const char *buf, unsigned int buflen,
	uint16_t offset, struct pkt_part *part)
{
	const char *fmt;
	struct pkt_rdata *rdata;
	uint16_t rlength;

	offset = pkt_name_unpack(buf, buflen, offset,
		&part->name);
	if (!offset)
		return 0;

	if (offset + 4 > buflen)
		return 0;
	SPEC_VALUE(part->type) = get16(buf + offset + 0);
	SPEC_SET_SPECIFIED(part->type);
	SPEC_VALUE(part->class) = get16(buf + offset + 2);
	SPEC_SET_SPECIFIED(part->class);
	offset += 4;

	if (part->section == SECTION_QU)
		return offset;

	if (offset + 6 > buflen) {
		verbose_printf("%s buf_to_pkt:"
				" short packet at <ttl,rlength>"
				" offset=%" PRIu16 " buflen=%" PRIu16 "\n",
				ERROR, offset, buflen);
		return 0;
	}
	SPEC_VALUE(part->ttl) = get32(buf + offset + 0);
	SPEC_SET_SPECIFIED(part->ttl);
	rlength = get16(buf + offset + 4);
	offset += 6;

	if (offset + rlength > buflen) {
		verbose_printf("%s buf_to_pkt:"
				"short packet at <rdata>"
				" offset=%" PRIu16 " buflen=%" PRIu16
				" rlength=%" PRIu16 "\n",
				ERROR, offset, buflen, rlength);
		return 0;
	}

	/* Truncate our view of the packet */
	buflen = offset + rlength;

	rdata = &SPEC_VALUE(part->rdata);
	SPEC_SET_SPECIFIED(part->rdata);
	fmt = table_to_str(rdata_format, SPEC_VALUE(part->type));
	if (!fmt)
		fmt = "x";
	while (offset < buflen) {
		char form;
		union pkt_rdata_member *m;
		uint16_t after;
		if (*fmt == '+')
			form = fmt[-1];
		else if (!*fmt)
			form = 'x';
		else
			form = *fmt++;
		m = pkt_rdata_member_new(rdata, form);
		after = pkt_rdata_member_unpack(buf, buflen, offset,
			form, m);
		if (!after) {
			verbose_printf("%s buf_to_pkt:"
				" short packet at <rdata:%c>"
				" offset=%" PRIu16 " rdata_end=%" PRIu16 "\n",
				ERROR, form, offset, buflen);
			return 0;
		}
		offset = after;
	}
	return offset;
}

/* unpacks buf into a pkt structure.
 * pkt must have been initialised by pkt_init() */
static int
buf_to_pkt(const char *buf, uint16_t buflen, struct pkt *pkt)
{
	unsigned int fi;
	unsigned int section;
	unsigned int i;
	uint16_t flags;
	uint16_t offset;

	if (buflen < DNS_HEADER_LEN)
		return 0;

	SPEC_SET_SPECIFIED(pkt->id);
	SPEC_VALUE(pkt->id) = get16(buf + DNS_HEADER_OFFSET_ID);

	flags = get16(buf + DNS_HEADER_OFFSET_FLAGS);
	for (fi = 0; fi < NFLAGS; fi++) {
		const struct flag_desc *desc = &pkt_flag_descs[fi];
		SPEC_SET_SPECIFIED(pkt->flag[fi]);
		SPEC_VALUE(pkt->flag[fi]) =
			(flags >> desc->shift) & desc->mask;
	}

	offset = DNS_HEADER_LEN;
	for (section = 0; section < NSECTIONS; section++) {
		uint16_t count = get16(buf + DNS_HEADER_OFFSET_COUNT(section));
		for (i = 0; i < count; i++) {
			struct pkt_part *part = pkt_part_new(pkt, section);
			offset = buf_to_pkt_part(buf, buflen, offset, part);
			if (!offset)
				return 0;
		}
	}
	if (offset < buflen) {
		verbose_printf("%s buf_to_pkt:"
			" unexpected trailing bytes"
			" offset=%" PRIu16 " buflen=%" PRIu16 "\n",
			ERROR, offset, buflen);
		return 0;
	}

	return 1;
}

static void
script_process_recv()
{
	char recv_data[9000];
	int recv_len;

	if (actual_sent_len != -1) {
		script_warn("previous sent packet unchecked (%d bytes)",
			actual_sent_len);
		pkt_fini(&recv_pkt);
	}

	/* construct a packet, "receive it" and then save the response
	 * in actual_sent_data[] */

	if (!script_can_eat('{'))
		script_error("expected '{' after 'recv'");
	verbose_printf("%s: recv { ... }\n", script_loc());
	script_eol();

	pkt_init(&recv_pkt, CONTEXT_RECV);
	while (!script_can_eat('}'))
		script_pkt_process_line(&recv_pkt);
	script_eol();

	if (verbose) {
		fprintf(stderr, "%s: recv_pkt = ", script_loc());
		pkt_print(stderr, &recv_pkt);
		fputc('\n', stderr);
	}

	/* Convert the recv{...} specification into a binary form,
	 * and pretend we just received it from the network. */
	recv_len = pkt_to_buf(&recv_pkt, recv_data, sizeof recv_data);
	verbose_printf("calling mdns_respond() with recv %d bytes\n",
		recv_len);
#if 0
	if (recv_len && verbose) {
		fprintf(stderr, "recv[] = \n");
		print_hexdump(stderr, recv_data, recv_len, 0);
	}
#endif

	/* Pass the "recv" packet to the function-under-test,
	 * and capture its output in actual_sent_data[] */
	actual_sent_len = mdns_respond(recv_data, recv_len,
		actual_sent_data, sizeof actual_sent_data, &recv_pkt.qinfo);

	verbose_printf("mdns_respond() actually returned send %d bytes\n",
		actual_sent_len);
	if (actual_sent_len && verbose) {
		fprintf(stderr, "actual_sent[] = \n");
		print_hexdump(stderr, actual_sent_data, actual_sent_len, 0);
	}
}

/*------------------------------------------------------------
 * sent pkt checking
 */

__attribute__((noreturn))
static void
check_error(const struct loc *loc, const char *fmt, ...)
{
	va_list ap;

	fprintf(stderr, "%s: FAIL: ", loc_to_str(loc));
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
	putc('\n', stderr);

	fputs("recv ", stderr);
	pkt_print(stderr, &recv_pkt);
	fputs("\nsent.expected ", stderr);
	pkt_print(stderr, &sent_pkt);
	fputs("\nsent.actual ", stderr);
	pkt_print(stderr, &actual_sent_pkt);
	putc('\n', stderr);

	exit(1);
}

static const char *
u16_to_str(uint16_t v)
{
	static char buf[20];
	snprintf(buf, sizeof buf, "%" PRIu16, v);
	return buf;
}

static int
u16_is_eq(uint16_t a, uint16_t b)
{
	return a == b;
}

static const char *
u32_to_str(uint32_t v)
{
	static char buf[20];
	snprintf(buf, sizeof buf, "%" PRIu32, v);
	return buf;
}

static int
u32_is_eq(uint32_t a, uint32_t b)
{
	return a == b;
}

static int
section_is_eq(unsigned char a, unsigned char b)
{
	return a == b;
}

static const char *
section_to_str(unsigned char section)
{
	return section < NSECTIONS ? section_name[section] : "?";
}

#define CHECK_T(expv, actv, name, exploc, is_eq, to_str)		\
    do {								\
	if (!is_eq(expv, actv)) {					\
		char expv_str[1024];					\
		char actv_str[1024];					\
		safe_strcpy(expv_str, to_str(expv));			\
		safe_strcpy(actv_str, to_str(actv));			\
		check_error(&(exploc),					\
		    "expected %s = %s"					\
		    ", actual = %s",					\
		    name, expv_str, actv_str);				\
	}								\
    } while (0)

#define CHECK_TF(form, expv, actv, name, exploc, is_eq, to_str)		\
    do {								\
	if (!is_eq(form, expv, actv)) {					\
		char expv_str[1024];					\
		char actv_str[1024];					\
		safe_strcpy(expv_str, to_str(form, expv));		\
		safe_strcpy(actv_str, to_str(form, actv));		\
		check_error(&(exploc),					\
		    "expected %s = %s"					\
		    ", actual = %s",					\
		    name, expv_str, actv_str);				\
	}								\
    } while (0)

#define SPEC_CHECK_T(exps, acts, name, exploc, is_eq, to_str)		\
    do {								\
	if (SPEC_IS_SPECIFIED(exps)) {					\
		if (!SPEC_IS_SPECIFIED(acts)) {				\
			char exp_str[1024];				\
			safe_strcpy(exp_str, to_str(SPEC_VALUE(exps))); \
			check_error(&(exploc),				\
			    "expected %s = %s"				\
			    ", actual not specified",			\
			    name, exp_str);				\
		}							\
		CHECK_T(SPEC_VALUE(exps), SPEC_VALUE(acts), name,	\
			exploc, is_eq, to_str);				\
	}								\
    } while (0)

#define SPEC_CHECK_U16(exps, acts, name, exploc)			\
	SPEC_CHECK_T(exps, acts, name, exploc, u16_is_eq, u16_to_str)
#define SPEC_CHECK_U32(exps, acts, name, exploc)			\
	SPEC_CHECK_T(exps, acts, name, exploc, u32_is_eq, u32_to_str)
#define CHECK_NAME(expv, actv, name, exploc)				\
	CHECK_T(&(expv), &(actv), name, exploc, pkt_name_is_eq,pkt_name_to_str)
#define CHECK_SECTION(expv, actv, name, exploc)				\
	CHECK_T(expv, actv, name, exploc, section_is_eq, section_to_str)
#define CHECK_RDATA_MEMBER(form, expv, actv, name, exploc)		\
	CHECK_TF(form, expv, actv, name, exploc,			\
		pkt_rdata_member_is_eq, pkt_rdata_member_to_str)

static void
pkt_check(const struct pkt *exp, const struct pkt *act)
{
	unsigned int p;
	unsigned int fi;

	/* check id */
	SPEC_CHECK_U16(exp->id, act->id, "id", exp->id_loc);

	/* check flags */
	for (fi = 0; fi < NFLAGS; fi++)
		SPEC_CHECK_U16(exp->flag[fi], act->flag[fi],
			pkt_flag_descs[fi].name, exp->flag_loc);

	/* check npart */
	if (exp->npart != act->npart)
		check_error(&exp->loc,
			"expected %" PRIu16 " parts, actual " PRIu16,
			exp->npart, act->npart);

	/* check each part */
	for (p = 0; p < exp->npart; p++) {
		const struct pkt_part *expp = &exp->part[p];
		const struct pkt_part *actp = &act->part[p];

		CHECK_SECTION(expp->section, actp->section, "section",
			expp->loc);
		CHECK_NAME(expp->name, actp->name, "name", expp->loc);
		SPEC_CHECK_U16(expp->class, actp->class, "class", expp->loc);
		SPEC_CHECK_U16(expp->type, actp->type, "type", expp->loc);
		if (expp->section == SECTION_QU)
			continue;
		SPEC_CHECK_U32(expp->ttl, actp->ttl, "ttl", expp->loc);

		/* check each component of the rdata */
		if (SPEC_IS_SPECIFIED(expp->rdata) &&
		    SPEC_IS_SPECIFIED(actp->rdata))
		{
			const struct pkt_rdata *expd = &SPEC_VALUE(expp->rdata);
			const struct pkt_rdata *actd = &SPEC_VALUE(actp->rdata);
			unsigned int i;

			if (expd->len != actd->len)
				check_error(&exp->loc,
					"expected %u rdata elements, "
					"actual %u", expd->len, actd->len);
			for (i = 0; i < expd->len; i++) {
				char form = expd->form[i];
				if (expd->form[i] != actd->form[i])
				    check_error(&exp->loc,
					    "expected rdata element '%c', "
					    "actual '%c",
					    expd->form[i], actd->form[i]);
				CHECK_RDATA_MEMBER(form, expd->member[i],
					actd->member[i], "rdata member",
					exp->loc);
			}
		}
	}
	verbose_printf("pkt_check() OK!\n");
}

static void
script_process_sent()
{
	if (actual_sent_len == -1)
		script_error("sent: already checked sent");

	pkt_init(&sent_pkt, CONTEXT_SENT);
	sent_pkt.loc = script.loc;

	if (!script_can_eat('{')) {
		const char *word = script_word();
		if (strcmp(word, "nothing") != 0)
			script_error("expected '{' or 'nothing'");
		verbose_printf("%s: sent nothing\n", script_loc());
		script_eol();

		sent_pkt.context = CONTEXT_INVALID;

		/* verify 'sent nothing' */
		if (actual_sent_len)
			script_error("sent nothing: FAIL"
				" because actually sent %d", actual_sent_len);
	} else {
		verbose_printf("%s: sent { ... }\n", script_loc());
		script_eol();

		/* verify we sent something */
		if (!actual_sent_len)
			script_error("FAIL because nothing was actually sent");

		while (!script_can_eat('}'))
			script_pkt_process_line(&sent_pkt);
		script_eol();

		if (verbose) {
			fprintf(stderr, "%s: sent_pkt = ", script_loc());
			pkt_print(stderr, &sent_pkt);
			fprintf(stderr, "\n");
		}

		/* Convert the actually_sent packet */
		pkt_init(&actual_sent_pkt, CONTEXT_SENT);
		buf_to_pkt(actual_sent_data, actual_sent_len,
			&actual_sent_pkt);
		if (verbose) {
			fprintf(stderr, "actual_sent_pkt = ");
			pkt_print(stderr, &actual_sent_pkt);
			fprintf(stderr, "\n");
		}

		pkt_check(&sent_pkt, &actual_sent_pkt);
		pkt_fini(&actual_sent_pkt);
	}

	pkt_fini(&sent_pkt);
	pkt_fini(&recv_pkt);
	clear_actual_sent();
}

/*------------------------------------------------------------
 * script line processing
 */

static void
script_process_line()
{
	const char *word;

	script_skip_space();
	if (script_at_eol()) {		/* blank lines ignored */
		script_eol();
		return;
	}

	/* Each script line begins with a 'command' word */
	word = script_word();
	script_skip_space();
	if (strcmp(word, "hostname") == 0) {
		script_process_hostname();
	} else if (strcmp(word, "ifaddr") == 0) {
		script_process_ifaddr();
	} else if (strcmp(word, "sleep") == 0) {
		script_process_sleep();
	} else if (strcmp(word, "ttl") == 0) {
		default_ttl = script_int();
		verbose_printf("%s: ttl %" PRIu32 "\n", script_loc(), default_ttl);
		script_eol();
	} else if (strcmp(word, "zones") == 0) {
		script_process_zones();
	} else if (strcmp(word, "recv") == 0) {
		script_process_recv();
	} else if (strcmp(word, "sent") == 0) {
		script_process_sent();
	} else {
		script_error("unknown command '%s'", word);
	}
}

static void
script_process_file(const char *filename)
{
	clear_actual_sent();

	script_open(filename);
	while (script.lookahead != EOF)
		script_process_line();

	/* Check for a recv{} without a sent{} */
	if (actual_sent_len != -1)
		script_warn("last sent packet was unchecked (%d bytes)",
			actual_sent_len);

	script_close();
}

/*------------------------------------------------------------
 * main
 */

int
main()
{
#if !NO_VERBOSE
	verbose = 1;
#endif

	script_process_file("respond-t.script");
	return 0;
}
