#pragma once

#include <inttypes.h>

struct sockaddr;

/* Handles a ready mDNS socket, using mdns_respond() to answer it */
void mdns_recv(int fd);
