/*
 * umDNSd - a small multicast DNS responder
 *
 * Reads standard zone files (RFC 1035) and responds to
 * mDNS (RFC 6762) queries on specified network interface.
 *
 * David Leonard, 2017.
 */

#include <err.h>
#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/poll.h>
#include <assert.h>

#include "verbose.h"
#include "mcast.h"
#include "mcast6.h"
#include "recv.h"
#include "zones.h"

#define ARRAY_SIZE(a)	(sizeof (a) / sizeof (a)[0])
#define MAX_HOSTFILES	32	/* Maximum number of -f arguments */
#define MAX_SOCK	 3

static void
exit_usage(int exit_code, const char *argv0)
{
	fprintf(stderr,
		"usage: %s [-46v] <interface> [<zonefile>...]\n"
		"", argv0);
	exit(exit_code);
}

volatile int reload;  /* also set by sighup() */
static void
sighup(int sig)
{
	reload = 1;
}

int
main(int argc, char *argv[])
{
	int ch;
	struct sigaction sa;
	unsigned int i;
	char **zonefiles, **zf;
	struct pollfd pfd[MAX_SOCK] = {};
	void (*pfd_recv[MAX_SOCK])(int);
	unsigned int npfd = 0;
	const char *interface;
	int opt4 = 0;
	int opt6 = 0;
	int fd;

	while ((ch = getopt(argc, argv, "46v")) != -1) {
		switch (ch) {
		case '4':
			opt4 = 1;	/* IPv4 only */
			break;
		case '6':
			opt6 = 1;	/* IPv6 only */
			break;
		case 'v':
#if NO_VERBOSE
			fprintf(stderr, "-v: not supported (NO_VERBOSE)\n");
#else
			verbose++;
#endif
			break;
		default:
			exit_usage(1, argv[0]);
			break;
		}
	}

	if (optind < argc)
		interface = argv[optind++];
	else
		exit_usage(1, argv[0]);

	zonefiles = argv + optind;

	if (!opt4 && !opt6)
		opt4 = opt6 = 1;	/* Default to both IPv4 and IPv6 */

	if (opt4 && (fd = mcast_socket(interface)) != -1) {
		pfd[npfd].fd = fd;
		pfd[npfd].events = POLLIN;
		pfd_recv[npfd] = mdns_recv;
		npfd++;
	}
	if (opt6 && (fd = mcast6_socket(interface)) != -1) {
		pfd[npfd].fd = fd;
		pfd[npfd].events = POLLIN;
		pfd_recv[npfd] = mdns_recv;
		npfd++;
	}
	assert(npfd <= MAX_SOCK);

	/* Arrange for SIGHUP to set reload=1 */
	memset(&sa, 0, sizeof sa);
	sa.sa_handler = sighup;
	if (sigaction(SIGHUP, &sa, NULL) == -1)
		err(1, "sigaction");

	/* Main loop */
	reload = 1;
	for (;;) {
		if (reload) {
			reload = 0;
			zones_clear();
			for (zf = zonefiles; *zf; zf++)
				zones_load(*zf);
		}
		if (poll(pfd, npfd, -1) == -1) {
			if (errno == EINTR)
				continue;
			err(1, "poll");
		}
		for (i = 0; i < npfd; i++) {
			if (!pfd[i].revents)
				continue;
			pfd_recv[i](pfd[i].fd);
		}
	}
}
