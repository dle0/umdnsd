/*
 * Respond to mDNS query packets.
 */

#include <stdio.h>
#include <string.h>
#include <err.h>
#include <sys/socket.h>	/* AF_INET */
#include <arpa/inet.h>
#include <arpa/nameser.h>

#include "zones.h"
#include "name.h"
#include "table.h"
#include "respond.h"
#include "rr.h"
#include "verbose.h"

/*
 * DNS packet format summary (see RFC 1035):
 *  The 12-byte DNS header is followed by the QD, AN, NS,
 *  and AR sections in that order, according to their count.
 *  A QD (question record) consists of a DNS name followed
 *  by a 16-bit type and a 16-bit class field.
 *  DNS names are encoded using compressed labels.
 *  The other sections (Answer, Namserver, Additional)
 *  begin with the same layout as a QD section, then are
 *  followed by a 32-bit TTL field and a variable-length
 *  RDATA field which begins with a 16-bit length.
 *
 * mDNS adds (RFC 6762):
 *  - The header's 16-bit ID field is ignored and set to 0.
 *  - The maximum size of a packet is 9000 bytes,
 *    including IP header (20) and UDP header (8).
 *  - The high bit 0x8000 of each record's type is reserved
 *    to indicate a record set.
 *  - The high bit 0x8000 of each record's class is reserved
 *    to indicate a cache flush (reply), or unicast desired (query).
 *    (QM is used to indicate a multicast query; and
 *     QU is used to indicate a unicast query)
 *  - query packets may contain 'known answers', and
 *    responders must be quiet if they agree with
 *    the answers AND the 'known answer's TTL is more
 *    than half of what the responder believes.
 *  - The Authority (NS) section is used for probe tiebreaking.
 *    (A probe is a unicast QU query where the NS section
 *    contains the proposed answer.)
 *
 *
 *
 * Like Bolo, the protocol may contain hidden mines.
 */

/*
 * DNS packet header
 * Decoded from network wire format and held in memory
 * in this struct, the fields are in host byte order.
 */
struct dns_header {
	uint16_t id;
	uint16_t flags;
	uint16_t qdcount, ancount, nscount, arcount;
};
#define FLAG_QR		(0x8000 >>  0)
#define FLAG_AA		(0x8000 >>  5)
#define FLAG_TC		(0x8000 >>  6)
#define FLAG_RD		(0x8000 >>  7)
#define FLAG_RA		(0x8000 >>  8)
#define GET_FLAG_OPCODE(h) (((h)->flags >> (15 -  4)) & 15)
#define GET_FLAG_RCODE(h)  (((h)->flags >> (15 - 15)) & 15)
#define SET_FLAG_OPCODE(h,c) \
	(h)->flags = ((h)->flags & ~0x7800) | (((c) & 15) << (15 -  4))
#define SET_FLAG_RCODE(h,c)  \
	(h)->flags = ((h)->flags & ~0x000f) | (((c) & 15) << (15 - 15))
#define DNS_PKT_HEADER_SIZE 12


/* Reads a 16-bit value out of an unaligned network packet. */
static uint16_t
get16(const void *p) {
	uint16_t u16;
	memcpy(&u16, p, sizeof u16);
	return ntohs(u16);
}

/* Stores a 16-bit value in network order.
 * Returns number of bytes stored */
static uint16_t
put16(uint16_t v, void *p) {
	uint16_t u16 = htons(v);
	memcpy(p, &u16, sizeof u16);
	return sizeof u16;
}

/* Reads a 32-bit value from an unaligned network packet. */
static uint32_t
get32(const void *p) {
	uint32_t u32;
	memcpy(&u32, p, sizeof u32);
	return ntohl(u32);
}

/* Stores a 32-bit number in network order.
 * Returns number of bytes stored */
static uint16_t
put32(uint16_t v, void *p) {
	uint32_t u32 = htonl(v);
	memcpy(p, &u32, sizeof u32);
	return sizeof u32;
}

/* Reads the DNS header from the beginning of a network packet.
 * The packed header is expanded into a host-endian struct dns_header.
 * Returns the number of bytes of packet header decoded.
 * Returns 0 on error. */
static uint16_t
read_header(const char *pkt, uint16_t pktlen, struct dns_header *hdr)
{
	if (pktlen < DNS_PKT_HEADER_SIZE)
		return 0;
	hdr->id = get16(pkt);
	hdr->flags = get16(pkt + 2);
	hdr->qdcount = get16(pkt + 4);
	hdr->ancount = get16(pkt + 6);
	hdr->nscount = get16(pkt + 8);
	hdr->arcount = get16(pkt + 10);
	return DNS_PKT_HEADER_SIZE;
}

/* Writes a DNS header at the beginning of a network packet buffer.
 * The struct dns_header is encoded into a packed, network-endian form.
 * Returns the offset position after the header, or
 * returns 0 if there is insufficient size. */
static uint16_t
write_header(char *pkt, uint16_t pktsz, const struct dns_header *hdr)
{
	if (pktsz < DNS_PKT_HEADER_SIZE)
		return 0;
	pkt += put16(hdr->id, pkt);
	pkt += put16(hdr->flags, pkt);
	pkt += put16(hdr->qdcount, pkt);
	pkt += put16(hdr->ancount, pkt);
	pkt += put16(hdr->nscount, pkt);
	pkt += put16(hdr->arcount, pkt);
	return DNS_PKT_HEADER_SIZE;
}

/* Prints a DNS packet to stdout for debugging. */
static void
debug_print_dns_packet(const char *pkt, uint16_t len)
{
	struct dns_header hdr;
	unsigned count;
	uint16_t pos, i;
	uint8_t opcode, rcode;

	pos = read_header(pkt, len, &hdr);
	if (!pos) {
		printf("packet too short (%u)\n", len);
		return;
	}

	#define BOOL(v) ((v) ? '1' : '0')

	printf(" ID:      %04x\n", hdr.id);
	printf(" QR:         %c\n", BOOL(hdr.flags & FLAG_QR));
	opcode = GET_FLAG_OPCODE(&hdr);
	printf(" Opcode:    %2u %s\n", opcode,
		opcode == QUERY ? "QUERY" :
		opcode == IQUERY ? "IQUERY" :
		opcode == STATUS ? "STATUS" :
		"");
	printf(" AA:         %c\n", BOOL(hdr.flags & FLAG_AA));
	printf(" TC:         %c\n", BOOL(hdr.flags & FLAG_TC));
	printf(" RD:         %c\n", BOOL(hdr.flags & FLAG_RD));
	printf(" RA:         %c\n", BOOL(hdr.flags & FLAG_RA));
	rcode = GET_FLAG_RCODE(&hdr);
	printf(" Rcode:     %2u %s\n", rcode,
		rcode == NOERROR  ? "NOERROR" :
		rcode == FORMERR  ? "FORMERR" :
		rcode == SERVFAIL ? "SERVFAIL" :
		rcode == NXDOMAIN ? "NXDOMAIN" :
		rcode == NOTIMP   ? "NOTIMP" :
		rcode == REFUSED  ? "REFUSED" :
		"");

	#undef BOOL

	printf(" Counts   { ");
	if (hdr.qdcount)
		printf("qd=%u ", hdr.qdcount);
	if (hdr.ancount)
		printf("an=%u ", hdr.ancount);
	if (hdr.nscount)
		printf("ns=%u ", hdr.nscount);
	if (hdr.arcount)
		printf("ar=%u ", hdr.arcount);
	printf("}\n");

	/* Print each section after the header */
	count = hdr.qdcount + hdr.ancount + hdr.nscount + hdr.arcount;
	while (count--) {
		unsigned is_question = 0;
		_DN char name[NAME_MAX];
		struct rr rr;
		const char *typename;
		uint16_t epos;

		if (hdr.qdcount) {
			printf("QUESTION:   ");
			hdr.qdcount--;
			is_question = 1;
		} else if (hdr.ancount) {
			printf("ANSWER:     ");
			hdr.ancount--;
		} else if (hdr.nscount) {
			printf("NAMESERVER: ");
			hdr.nscount--;
		} else {
			printf("ADDITIONAL: ");
			hdr.arcount--;
		}

		/* Print the name. */
		pos = name_unpack(pkt, len, pos, name);
		if (!pos) {
			printf("error unpacking name\n");
			break;
		}
		printf("%s ", name_repr(name));

		/* Decode the type and class. */
		if (pos + sizeof rr.rr_type + sizeof rr.rr_class > len) {
			printf("truncated packet\n");
			break;
		}
		rr.rr_type = get16(pkt + pos);
		pos += sizeof (uint16_t);
		rr.rr_class = get16(pkt + pos);
		if (rr.rr_class & 0x8000) {
			rr.rr_class &= ~0x8000;
			if (is_question) {
				printf("[unicast] ");
			} else {
				printf("[rrset] ");
			}
		}
		pos += sizeof (uint16_t);

		/* Decode and print the TTL. */
		if (!is_question) {
			if (pos + sizeof rr.rr_ttl + sizeof rr.rr_len > len) {
				printf("truncated packet\n");
				break;
			}
			rr.rr_ttl = get32(pkt + pos);
			pos += sizeof (uint32_t);
			rr.rr_len = get16(pkt + pos);
			pos += sizeof (uint16_t);

			/* Show extended DNS options for debug */
			if (rr.rr_type == T_OPT)
				printf("[r=%x v=%u z=%x] ",
					(rr.rr_ttl >> 24) & 0xf,
					(rr.rr_ttl >> 16) & 0xf,
					(rr.rr_ttl >> 0) & 0xff);
			else
				printf("%u ", rr.rr_ttl);
		} else
			rr.rr_len = 0;

		/* Print the class and type. */
		if (rr.rr_type == T_OPT)
			printf("[udp %d] ", rr.rr_class);
		else if (rr.rr_class == C_IN)
			printf("IN ");
		else
			printf("CLASS%d ", rr.rr_class);

		typename = table_to_str(rr_type_table, rr.rr_type);
		if (typename)
			printf("%s ", typename);
		else
			printf("TYPE%d ", rr.rr_type);

		/* Print the resource data */
		if (is_question) {
			/* But question sections don't have RDATA */
			putchar('\n');
			continue;
		}
		epos = pos + rr.rr_len;
		switch (rr.rr_type) {
		case T_A:
		{	char tmp[INET_ADDRSTRLEN];
			if (rr.rr_len == 4 && inet_ntop(AF_INET, pkt + pos,
				tmp, sizeof tmp))
				printf("%s\n", tmp);
			else
				printf("[bad]\n");
		}	break;
		case T_AAAA:
		{	char tmp[INET6_ADDRSTRLEN];
			if (rr.rr_len == 16 && inet_ntop(AF_INET6, pkt + pos,
				tmp, sizeof tmp))
				printf("%s\n", tmp);
			else
				printf("[bad]\n");
		}	break;
		case T_PTR:
			if (name_unpack(pkt, len, pos, name) == epos)
				printf("%s\n", name_repr(name));
			else
				printf("[bad]\n");
			break;
		case T_NSEC:
			pos = name_unpack(pkt, len, pos, name);
			if (pos && pos < epos) {
				printf("%s (", name_repr(name));
				while (pos + 3 < epos) {
				    uint16_t bnum = (uint8_t)pkt[pos++] << 8;
				    uint16_t blen = (uint8_t)pkt[pos++], b;
				    if (blen > 32) goto bad_nsec;
				    for (b = 0; b < blen*8; b++) {
					if (pkt[pos+b/8] & (0x80>>(b%8))) {
					    uint16_t type = b + bnum;
					    const char *tn = table_to_str(
						rr_type_table, type);
					    if (tn) printf(" %s", tn);
					    else    printf(" TYPE%u", type);
					}
				    }
				    pos += blen;
				}
				if (pos != epos) printf("[SHORT]");
				printf(" )\n");
			} else
				bad_nsec: printf("[bad name]\n");
			break;
		default:
			printf("\\# %u ", rr.rr_len);
			for (i = rr.rr_len; i--; ) {
				if (pos == len) {
					printf("truncated packet\n");
					return;
				}
				printf("%02x", pkt[pos++] & 0xff);
			}
			putchar('\n');
		}
		pos = epos;
	}
	if (pos < len)
		printf("  ; %u bytes left in packet\n", len - pos);
}

/* Prints a hex dump to stdout. For debugging. */
static void
debug_print_hex(const void *data, unsigned int len, unsigned int offset)
{
	unsigned int i;

	for (i = 0; i < len; i++) {
		if ((i & 15) == 0)
			printf("%04x: ", i + offset);
		if ((i & 7) == 0)
			putchar(' ');
		printf(" %02x", ((unsigned char *)data)[i]);
		if ((i & 15) == 15 || i == len - 1)
			putchar('\n');
	}
	putchar('\n');
}

/* Tests if the name matches
 *   <svc>._tcp.<domain> or
 *   <svc>._udp.<domain>
 * where <svc> is a simple label,
 *   and <domain> is any sequence of labels.
 * Returns pointer to <domain> if so, or
 * returns NULL if no match.
 */
static const _DN char *
is_service_domain(const _DN char *name)
{
	static _DN char tcp_label[] = "\04_tcp";
	static _DN char udp_label[] = "\04_tcp";
	static unsigned int label_len = 5;

	if (!*name)
		return NULL;
	name += 1 + *name; /* skip first label */
	if (memcmp(name, tcp_label, label_len) == 0 ||
	    memcmp(name, udp_label, label_len) == 0)
	{
		name += label_len;
		if (*name)
			return name;
	}
	return NULL;
}

/* Tests if the name matches
 *    <label>+.<domain>
 * Returns 0 iff not. */
static int
is_subdomain(const _DN char *name, const _DN char *domain)
{
	while (*name) {
		name += 1 + *name;
		if (name_eq(name, domain))
			return 1;
	}
	return 0;
}

/* Tests if the name matches
 *     _services._dns-sd._udp.<domain>.
 * Returns pointer to <domain> if it does, or
 * returns NULL if no match. */
static const _DN char *
is_services_dns_sd_domain(const _DN char *name)
{
	static _DN char prefix[] =
		"\011_services"
		"\007_dns-sd"
		"\004_udp";
	static unsigned int prefix_len = sizeof prefix - 1;

	if (memcmp(name, prefix, prefix_len) != 0)
		return NULL;
	if (!name[prefix_len])
		return NULL;
	return name + prefix_len;
}

/* Tests if the rr is a PTR to a name matching
 *     <svc>._tcp.<domain> or
 *     <svc>._udp.<domain>.
 * Returns 0 iff not. */
static int
is_dns_sd_ptr(const struct rr *rr, const _DN char *domain)
{
	uint16_t i;
	const char *rdata;

	if (rr->rr_type != T_PTR)
		return 0;
	rdata = rr->rr_data;
	i = 0;
	if (rdata[i] == 0)	/* PTR . */
		return 0;
	i += 1 + rdata[i];
	if (rdata[i] != 4)
		return 0;
	i++;
	if (memcmp(rdata + i, "_tcp", 4) != 0 &&
	    memcmp(rdata + i, "_udp", 4) != 0)
		return 0;
	i += 4;
	return name_eq((const _DN char *)(rdata + i), domain);
}

struct response {
	struct dns_header hdr;
	uint16_t offsets[NAME_PACK_OFFSETS];
	uint16_t pos;
	void *buf;
	uint16_t bufsz;
	const struct query_info *qinfo;
};

/* Appends an answer section to a DNS packet buffer.
 * Suitable for use with AN, NS and AR sections.
 * Appends: NAME TYPE CLASS TTL RDLENGTH RDATA.
 * Returns the offset after the packed data, or
 * returns 0 if the buffer was too small. */
static uint16_t
answer_pack(struct response *response,
	const _DN char *name, const struct rr *rr)
{
	uint32_t ttl;
	uint16_t pos = response->pos;
	uint16_t after;

	/* Append an answer section */
	pos = name_pack(response->buf, response->bufsz, pos, name, response->offsets);
	if (!pos || pos + 2 + 2 + 4 + 2 > response->bufsz) {
		warnx("reply too big");
		response->pos = 0;
		return 0;
	}
	pos += put16(rr->rr_type, response->buf + pos);
	pos += put16(rr->rr_class, response->buf + pos);

	if (!response->qinfo->unicast || rr->rr_ttl < 10)
		ttl = rr->rr_ttl;
	else
		ttl = 10; /* low TTL when unicasting (rfc6762 s6.7) */
	pos += put32(ttl, response->buf + pos);

	after = rr_pack_data(response->buf, response->bufsz, pos + 2,
		rr, response->offsets);
	if (!after)
		warnx("reply too big");
	put16(after - (pos + 2), response->buf + pos); /* rlength */
	response->pos = after;
	return after;
}

/* Input packet context, used for seeking and holding scanned data */
struct inctx {
	struct dns_header hdr;	/* decoded header */
	uint16_t pos;		/* current seek position */
	uint16_t anpos;		/* beginning of answers (hdr.ancount) */
	const void *pkt;	/* packet data */
	uint16_t pktlen;
};

/* A <name,type,class> tuple */
struct name_type_class {
	_DN char name[NAME_MAX];
	uint16_t type;
	uint16_t class;
};

struct ttl_rdata {
	uint32_t ttl;
	uint16_t rlength;
	uint16_t rdata;
};

/* Scans the next <name,type,class> sequence from a DNS packet.
 * Stores the scanned tuple in ntc_return (unless it is NULL).
 * Advances the input context.
 * Returns 0 on failure. */
static int
inctx_scan_name_type_class(struct inctx *in, struct name_type_class *ntc_return)
{
	in->pos = name_unpack(in->pkt, in->pktlen, in->pos,
		ntc_return ? ntc_return->name : NULL);
	if (!in->pos)
		return 0; /* Bad name */
	if (in->pos + sizeof (uint16_t) + sizeof (uint16_t) > in->pktlen)
		return 0; /* Truncated packet */
	if (ntc_return) {
		ntc_return->type = get16(in->pkt + in->pos);
		ntc_return->class = get16(in->pkt + in->pos + sizeof (uint16_t));
	}
	in->pos += sizeof (uint16_t) + sizeof (uint16_t);
	return 1;
}

/* Scans the next <name,type,class,ttl,rdata> entry from the packet
 * and compares it to the given <name,rr>, ignoring the TTL.
 * Advances *in to the next record after scanning <name,type,class,ttl,rdata>.
 * Returns 1 if the record matches.
 * Returns 0 if the record did not match.
 * Returns -1 if there was a malformed packet. */
static int
inctx_match_name_rr(struct inctx *in, const _DN char *name, const struct rr *rr)
{
	int same;
	uint16_t rlength;

	/* Compare <name> */
	same = name_packed_eq(name, in->pkt, in->pktlen, in->pos, &in->pos);
	if (!in->pos)
		return -1;

	/* Check early that there is room for <type,class,ttl,rlength> */
	if (in->pos + sizeof (uint16_t) + sizeof (uint16_t) +
	              sizeof (uint32_t) + sizeof (uint16_t) > in->pktlen)
		return -1;

	/* Compare <type> */
	if (same) same = get16(in->pkt + in->pos) == rr->rr_type;
	in->pos += sizeof (uint16_t);

	/* Compare <class> */
	if (same) same = get16(in->pkt + in->pos) == rr->rr_class;
	in->pos += sizeof (uint16_t);

	/* Skip <ttl> */
	/* ttl = get32(in->pkt + in->pos); */
	in->pos += sizeof (uint32_t);

	/* Compare <rlength> */
	rlength = get16(in->pkt + in->pos);
	if (same) same = rlength == rr->rr_len;
	in->pos += sizeof(uint16_t);

	/* Compare <rdata> */
	if (in->pos + rlength > in->pktlen)
		return -1;
	if (same) {
		same = rr_eq_data(in->pkt, in->pktlen, in->pos, rlength,
			rr, &in->pos);
		if (!in->pos)
			return -1;
	}
	in->pos += rlength;

	return same;
}

/* Initialises a DNS packet input context.
 * Decodes the packet header into in->hdr and leaves the context
 * pointing at the first byte after the header.
 * Returns 0 on failure */
static int
inctx_init(struct inctx *in, const void *pkt, uint16_t pktlen)
{
	in->pkt = pkt;
	in->pktlen = pktlen;
	in->pos = read_header(in->pkt, in->pktlen, &in->hdr);
	return in->pos;
}

/* Sets the seek position of the input context.
 * The special position 0 is interpreted to mean the
 * first entry just after the header.
 * Returns the old position. */
static uint16_t
inctx_seek(struct inctx *in, uint16_t newpos)
{
	uint16_t oldpos = in->pos;
	in->pos = newpos ? newpos : DNS_PKT_HEADER_SIZE;
	return oldpos;
}

/* Tests if the DNS question <type,class> matches a zone's RR
 * using standard matching criteria */
static int
qtype_class_matches_rr(uint16_t qtype, uint16_t qclass, const struct rr *rr)
{
	/* From RFC6762 section 6:
	 *  "the record name must match the question name,
	 *   the record rrtype must match the question qtype
	 *   unless the qtype is ANY (255) or the rrtype
	 *   is CNAME (5), and the record rrclass must match
	 *   the question qclass unless the qclass ANY (255).
	 */
	return (qclass == C_ANY || qclass == rr->rr_class) &&
	       (qtype == T_ANY  || qtype == rr->rr_type || T_CNAME == rr->rr_type);
}

/* Search the input packet for an answer record that matches <q,rr>.
 * (Does not alter the input context's position.)
 * Returns 0 if no matching answer found.
 * Returns -1 on malformed in->pkt
 * Returns 1 on matching answer found. */
static int
find_answer(struct inctx *in, const struct name_type_class *q, const struct rr *rr)
{
	uint16_t save_pos;
	uint16_t ancount;
	int found = 0;

	if (!in->hdr.ancount)
		return 0;

	save_pos = inctx_seek(in, in->anpos);
	ancount = in->hdr.ancount;
	while (ancount--) {
		found = inctx_match_name_rr(in, q->name, rr);
		if (found)
			goto out;
	}

out:
	(void) inctx_seek(in, save_pos);
	return found;
}


/* Handles receipt of a DNS packet. Looks up each question
 * in our zone database and returns a response if we can answer.
 * Returns number of bytes stored in the return packet buf.
 * Returns 0 if the input packet cannot be answered.
 */
int
mdns_respond(const void *pkt, uint16_t pktlen,
	 void *buf, uint16_t bufsz, const struct query_info *qinfo)
{

	struct inctx in;

	struct response out;
	uint16_t i;

	out.buf = buf;
	out.bufsz = bufsz;
	out.qinfo = qinfo;
	out.pos = DNS_PKT_HEADER_SIZE;

	if (verbose) {
		debug_print_hex(pkt, pktlen, 0);
		debug_print_dns_packet(pkt, pktlen);
	}
	if (!inctx_init(&in, pkt, pktlen))
		return 0; /* Bad header */
	if (verbose)
		printf("%u question(s)\n", in.hdr.qdcount);

	if (in.hdr.flags & FLAG_QR)
		return 0; /* Ignore reply packets; we only handle queries */
	if (GET_FLAG_OPCODE(&in.hdr) != QUERY)
		return 0; /* Ignore non-standard queries (RFC6762 18.3) */
	if (GET_FLAG_RCODE(&in.hdr) != NOERROR)
		return 0; /* Ignore non-standard rcodes (RFC6762 18.11) */
	if (in.hdr.qdcount == 0)
		return 0; /* Ignore packets without any questions */

	/* NOTE: We SHOULD handle TC flag by waiting for more answers,
	 * but that complicates state. So we don't. RFC6762 18.5 */

	/* TODO: We SHOULD, but do not need to, delay responses by 20-120 ms.
	 * We must not delay authoritative unique answers, but we
	 * SHOULD delay DNS-SD responses. RFC 6762 6. */

	/* Prepare for known-Answer suppression: RFC6762 7.1
	 * Seek through to the beginning of the input answers section.
	 * Later, if we decide to answer we will check the answer section first.
	 */
	for (i = 0; i < in.hdr.qdcount; i++)
		if (!inctx_scan_name_type_class(&in, NULL))
			return 0;
	in.anpos = inctx_seek(&in, 0);

	/* Initialize the response packet */
	out.hdr.id = qinfo->unicast ? in.hdr.id : 0;
	out.hdr.flags = FLAG_QR | FLAG_AA; /* (RFC6762 18.2, 18.4) */
	out.hdr.qdcount = 0;
	out.hdr.ancount = 0;
	out.hdr.nscount = 0;
	out.hdr.arcount = 0;
	out.offsets[0] = 0;
	out.pos = DNS_PKT_HEADER_SIZE;

	/* Start working our way through the questions */
	in.pos = DNS_PKT_HEADER_SIZE;
	for (; in.hdr.qdcount; in.hdr.qdcount--) {
		struct name_type_class q;
		const struct rr *rr;
		struct zone_rr *zrr;
		const _DN char *domain;
		const _DN char *svcdomain;

		/* Scan in question q = <name,type,class> */
		if (!inctx_scan_name_type_class(&in, &q))
			return 0; /* Malformed */

		/* Search zone database for an exact match. */
		for (zrr = zones_lookup(q.name); zrr; zrr = zrr->next) {
			const struct rr *rr = &zrr->rr;

			if (!qtype_class_matches_rr(q.type, q.class, rr))
				continue;

			if (find_answer(&in, &q, rr) == 1)
				continue;

			out.pos = answer_pack(&out, q.name, rr);
			if (!out.pos)
				return 0;
			out.hdr.ancount++;
		}

		/* Answer _services._dns-sd.udp.local. PTR request */
		if (q.type == T_PTR && (domain = is_services_dns_sd_domain(q.name))) {
			void *iter = NULL;
			const _DN char *n;

			while ((n = zones_name_next(&iter))) {
				for (zrr = zones_lookup(q.name); zrr; zrr = zrr->next) {
					rr = &zrr->rr;
					if (is_dns_sd_ptr(rr, domain)) {
						out.pos = answer_pack(&out,
							n, rr);
						if (!out.pos)
							return 0;
						out.hdr.ancount++;
					}
				}
			}
		}

		/* Answer <svc>.<domain> PTR request by looking for DNS records */
		if (q.type == T_PTR && (svcdomain = is_service_domain(q.name))) {
			void *iter = NULL;
			const _DN char *n;

			while ((n = zones_name_next(&iter))) {
				if (!is_subdomain(n, svcdomain))
					continue;
				for (zrr = zones_lookup(q.name); zrr; zrr = zrr->next) {
					rr = &zrr->rr;
					if (rr->rr_type == T_SRV) {
						out.pos = answer_pack(&out,
							n, rr);
						if (!out.pos)
							return 0;
						out.hdr.ancount++;
					}
				}
			}
		}
	}
	if (verbose)
		printf("%u answer(s)\n", out.hdr.ancount);
	if (!out.hdr.ancount)
		return 0; /* no answers */
	if (!write_header(out.buf, out.bufsz, &out.hdr))
		return 0;
	return out.pos;
}
