/*
 * Generic lookup table utilities
 */

#include <inttypes.h>
#include <string.h>
#include "table.h"

int
table_to_val(const struct table *t, const char *str, unsigned int *value_ret)
{
	for (; t->str; t++) {
		if (strcasecmp(str, t->str) == 0) {
			if (value_ret)
				*value_ret = t->value;
			return 1;
		}
	}
	return 0;
}

int
table_to_val16(const struct table *t, const char *str, uint16_t *value_ret)
{
	unsigned int value;
	int ret = table_to_val(t, str, &value);
	if (ret)
		*value_ret = value;
	return ret;
}

const char *
table_to_str(const struct table *t, unsigned int value)
{
	for (; t->str; t++)
		if (t->value == value)
			break;
	return t->str;
}
