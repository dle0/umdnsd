/*
 * Receive and respond to mDNS query packets.
 */

#include <stdio.h>
#include <string.h>
#include <err.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "recv.h"
#include "respond.h"
#include "verbose.h"
#include "mcast.h"
#include "mcast6.h"

/* Returns a readable representation of a sockaddr, for humans. */
static const char *
sockaddr_str(const struct sockaddr *sa)
{
	static char buf[INET6_ADDRSTRLEN + sizeof ",65536"] = "";
	const struct sockaddr_in *sin;
	const struct sockaddr_in6 *sin6;
	in_port_t port = 0;
	int len;

	switch (sa->sa_family) {
	case AF_INET:
		sin = (const struct sockaddr_in *)sa;
		if (!inet_ntop(AF_INET, &sin->sin_addr, buf, sizeof buf))
			return NULL;
		port = htons(sin->sin_port);
		break;
	case AF_INET6:
		sin6 = (const struct sockaddr_in6 *)sa;
		if (!inet_ntop(AF_INET, &sin6->sin6_addr, buf, sizeof buf))
			return NULL;
		port = htons(sin6->sin6_port);
		break;
	default:
		snprintf(buf, sizeof buf, "<af %d>", sa->sa_family);
		return buf;
	}
	len = strlen(buf);
	snprintf(buf + len, sizeof buf - len, ",%u", port);
	return buf;
}

static in_port_t
sockaddr_port(const struct sockaddr *sa)
{
	switch (sa->sa_family) {
	case AF_INET:
		return htons(((const struct sockaddr_in *)sa)->sin_port);
	case AF_INET6:
		return htons(((const struct sockaddr_in6 *)sa)->sin6_port);
	default:
		return 0;
	}
}

void
mdns_recv(int fd)
{
	/* Packet maximum is 9000 less the IP and UDP headers */
	unsigned char buf[9000 - 20 - 8];
	unsigned char wbuf[9000 - 20 - 8];
	struct sockaddr_storage from;
	socklen_t fromlen = sizeof from;
	int len, wlen;
	struct query_info qinfo;

	/* Receive query */
	len = recvfrom(fd, buf, sizeof buf, 0,
		(struct sockaddr *)&from, &fromlen);
	if (len == -1) {
		warn("recvfrom");
		return;
	}
	if (clock_gettime(CLOCK_MONOTONIC, &qinfo.time) == -1) {
		warn("clock_gettime");
		return;
	}
	if (verbose) {
		printf("%d bytes from %s\n", len,
			sockaddr_str((struct sockaddr *)&from));
	}

	/* If the origin port was not 5353, then it is a unicast request */
	qinfo.unicast = (sockaddr_port((struct sockaddr *)&from) != 5353);

	/* Process the query */
	wlen = mdns_respond(buf, len, wbuf, sizeof wbuf, &qinfo);
	if (wlen <= 0)
		return;

	if (!qinfo.unicast) {
		int ret;
		int (*mcast_sendfn)(int, const void *, unsigned int);

		switch (from.ss_family) {
		case AF_INET:	mcast_sendfn = mcast_send; break;
		case AF_INET6:	mcast_sendfn = mcast6_send; break;
		default:	errx(1, "unknown family %u", from.ss_family);
				return;
		}

		/* multicast reply */
		ret = mcast_sendfn(fd, wbuf, wlen);
		if (ret == -1)
			warn("sendto");
	} else {
		/* unicast reply */
		int ret = sendto(fd, wbuf, wlen, 0, (struct sockaddr *)&from, fromlen);
		if (ret == -1)
			warn("sendto");
	}
}
