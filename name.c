/*
 * Domain name labels: coding and compression
 * RFC 1035
 */

#include <stdio.h>
#include <inttypes.h>
#include <string.h>
#include <ctype.h>

#include "name.h"

const char *name_error = NULL;

/* Returns the size of an uncompressed valid label */
static unsigned int
label_size(const _DN char *label)
{
	if (*label & 0xc0)
		return 0;
	return 1 + *(unsigned char *)label;
}

/* Convert c to lowercase when US-ASCII 'A'..'Z' */
static unsigned char
lc(unsigned char c)
{
	if (c >= 'A' && c <= 'Z')
		return c - 'A' + 'a';
	else
		return c;
}

uint8_t
name_size(const _DN char *name)
{
	unsigned int l;
	unsigned int size = 0;

	while ((l = label_size(name + size))) {
		size += l;
		if (size > NAME_MAX)
			break;
		if (l == 1)
			return size;
	}
	return 0;
}

struct label_context {
	const char *pkt;
	uint16_t pktlen;
	uint16_t offset;  /* current offset */
	uint16_t after;   /* offset following compressed label */
};

/* Label decompression step. Checks for label validity,
 * advances lc->offset and maxmises lc->after (so it is
 * always pointing just after the decompressed label).
 * Returns a pointer to next label, or
 * returns NULL on invalid input. */
const _DN char *
label_step(struct label_context *lc)
{
	uint8_t b;
	uint16_t pos;

	pos = lc->offset;
	if (pos >= lc->pktlen)
		return NULL;
	b = lc->pkt[pos];

	/* Compressed label reference */
	if ((b & 0xc0)) {
		uint16_t nextpos;

		if ((b & 0xc0) != 0xc0)
			return NULL;
		if (pos + 1 >= lc->pktlen)
			return NULL;

		nextpos = (b & 0x3f) << 8 | (lc->pkt[pos + 1] & 0xff);
		if (nextpos >= pos)
			return NULL; /* cannot branch forward/self */
		if (nextpos >= lc->pktlen)
			return NULL; /* cannot branch beyond packet */

		/* Adjust lc->after */
		if (pos + 2 > lc->after)
			lc->after = pos + 2;

		pos = nextpos;
		b = lc->pkt[pos];
	}

	if (pos < 12)
		return NULL; /* suspicious label in header */
	if (pos + b + 1 > lc->pktlen)
		return NULL; /* label too big */

	lc->offset = pos + b + 1;

	/* Adjust lc->after */
	if (lc->offset > lc->after)
		lc->after = lc->offset;

	return (const _DN char *)&lc->pkt[pos];
}

uint16_t
name_unpack(const char *pkt, uint16_t pktlen, uint16_t offset, _DN char *name)
{
	struct label_context context = {
		.pkt = pkt,
		.pktlen = pktlen,
		.offset = offset,
		.after = 0,
	};

	unsigned int cnt;
	uint16_t namesz = 0;

	for (cnt = 0; cnt < 128; cnt++) {	/* maximum labels */
		const _DN char *label;
		unsigned int lsz;

		label = label_step(&context);
		if (!label)
			break;			/* malformed label */
		lsz = label_size(label);
		if (namesz + lsz > NAME_MAX)
			break;			/* name too long */
		if (name)
			memcpy(name + namesz, label, lsz);
		else if (context.offset < offset)
			return context.after;	/* shortcut when skipping name */
		namesz += lsz;
		if (lsz == 1)
			return context.after;
	}
	return 0;
}

int
name_store(_DN char *n, const _DN char *origin, const char *s)
{
	return name_storen(n, origin, s, strlen(s));
}

int
name_storen(_DN char *n, const _DN char *origin, const char *s, unsigned int s_len)
{
	unsigned int len = 0;
	const char *s_end = s + s_len;

	name_error = NULL;

	if (s_len == 0) {
		name_error = "missing name";
		goto error;
	}

	if (s_len == 1 && *s == '.') {
		/* special zone . */
		*n = 0;
		return 0;
	}

	if (s_len == 1 && *s == '@') {
		/* special zone @ */
		if (!origin) {
			name_error = "@ with no origin";
			goto error;
		}
		memcpy(n, origin, name_size(origin));
		return 0;
	}

	for (;;) {
		/* Read a label up to and including the next '.' */
		unsigned llen = 0;
		int dot;

		if (len >= NAME_MAX) {
			name_error = "name too long";
			goto error;
		}

		while (s < s_end && *s != '.') {
			char ch = *s++;
			if (ch == '\\') {
				/* handle \. \\ and \octal */
				if (s == s_end) {
					name_error = "name ends with \\";
					goto error;
				}
				if (s + 2 < s_end &&
				    isdigit(s[0]) &&
				    isdigit(s[1]) &&
				    isdigit(s[2]))
				{
					ch = (s[0] - '0') * 100 +
					     (s[1] - '0') * 10 +
					     (s[2] - '0');
					s += 3;
				} else
					ch = *s++;
			}
			++llen;
			if (len + llen >= NAME_MAX) {
				name_error = "name too long";
				goto error;
			}
			n[len + llen] = lc(ch);
		}
		if (llen > LABEL_MAX) {
			name_error = "label too long";
			goto error;
		}

		dot = (s < s_end) && (*s == '.');
		if (dot) s++;

		n[len] = llen;
		len += llen + 1;

		if (!dot && s != s_end) {
			name_error = "unknown trailer";
			goto error;
		}

		if (dot && llen == 0) {
			name_error = "missing name between dots";
			goto error; /* a.b..c: double dot */
		}

		if (!dot && llen == 0) {
			/* a.b.c."" terminated by a dot: fully qualified */
			break;
		}

		if (!dot) {
			/* a.b."c" not terminated by dot: append origin */
			unsigned int origin_size;
			if (!origin) {
				name_error = "relative name with no origin";
				goto error;
			}
			origin_size = name_size(origin);
			if (len + origin_size > NAME_MAX)
				goto error; /* too long */
			memcpy(n + len, origin, origin_size);
			len += origin_size;
			break;
		}
	}
	return 0;

error:
	return -1;
}

/* Compares two labels, ASCII-case insensitively.
 * No need to do Unicode comparison because of RFC5752 section 16:
 *  "...all names in Multicast DNS MUST be encoded as precomposed UTF-8
 *   [RFC3629] "Net-Unicode" [RFC5198] text. ... US-ASCII being
 *   considered a compatible subset of UTF-8...
 *   The simple rules for case-insensitivity ... also apply in
 *   Multicast DNS; that is to say, in name comparisons,
 *   the lowercase letters ['a'..'z'] match their uppercase
 *   equivalents ['A'..'Z'] ... No other automatic equivalences should
 *   be assumed. In particular, all UTF-8 multibyte characters ...
 *   (codes 0x80 and higher) are compared by simple binary comparison of
 *   the raw byte values."
 * The assumption is that the input names are 'precomposed', that is
 * normalised per RFC5198 section 3.
 */
static int
label_eq_caseless(const _DN char *n1, const _DN char *n2)
{
	uint8_t len = *n1++;

	if (*n2++ != len)
		return 0;
	for (; len--; n1++, n2++) {
		if (*n1 == *n2)
			continue;
		if (lc(*n1) == lc(*n2))
			continue;
		return 0;
	}
	return 1;
}

int
name_eq(const _DN char *n1, const _DN char *n2)
{
	uint8_t n1_size = name_size(n1);
	uint8_t n2_size = name_size(n2);

	return n1_size == n2_size &&
	       memcmp(n1, n2, n1_size) == 0;
}

int
name_packed_eq(const _DN char *n1, const void *pkt, uint16_t pktlen,
	uint16_t offset_n2, uint16_t *after_n2)
{
	struct label_context context = {
		.pkt = pkt,
		.pktlen = pktlen,
		.offset = offset_n2,
		.after = 0,
	};
	const _DN char *label;

	while ((label = label_step(&context))) {
		if (!label_eq_caseless(n1, label))
			break;
		if (!*n1) {
			if (after_n2)
				*after_n2 = context.after;
			return 1;
		}
		n1 += label_size(label);
	}

	if (after_n2) {
		/* Caller still wants the offset after the name,
		 * even when the match failed. */
		while (label && label_size(label) != 1)
			label = label_step(&context);
		*after_n2 = label ? context.after : 0;
	}

	return 0;
}

const char *
name_repr(const _DN char *n)
{
	unsigned int len, i;
	static char buf[4*255 + 1];
	char * const buf_end = &buf[sizeof buf];
	char *p = buf;

	if (!n)
		return "(null)";
	if (!*n)
		return ".";
	do {
		if (*n & 0xc0)
			return "(compressed)";
		len = *n++;
		if (p + len + 2 > buf_end)
			return "(overlong)";
		for (i = len; i--; ) {
			char ch = *n++;
			if (ch == '.' || ch == '\\') {
				*p++ = '\\';
				*p++ = ch;
			} else if (ch <= ' ' || ch >= 0x7f) {
				*p++ = '\\';
				*p++ = '0' + (ch / 100) % 10;
				*p++ = '0' + (ch /  10) % 10;
				*p++ = '0' + (ch      ) % 10;
			} else {
				*p++ = ch;
			}
		}
		*p++ = '.';
	} while (*n);
	if (p + 1 > buf_end)
		return "(overlong)";
	*p = '\0';
	return buf;
}

static int
match_at(const char *pkt, uint16_t offset, const _DN char *name)
{
	if (!*name)
		return 0;
	while (*name) {
		unsigned int len = name[0] + 1;
		if (memcmp(pkt + offset, name, len) != 0)
			return 0;
		offset += len;
		name += len;
		if ((pkt[offset] & 0xc0) == 0xc0)
			offset = (pkt[offset] << 8 | pkt[offset + 1]) & ~0xc000;
	}
	return 1;
}

uint16_t
name_pack(char *pkt, uint16_t pktsz, uint16_t offset,
	const _DN char *nd,
	uint16_t offsets[static NAME_PACK_OFFSETS] /* may be NULL */)
{
	unsigned int noff;    /* valid offsets on entry */
	unsigned int app_off; /* append position */

	/* Determine how many recorded offsets we have */
	for (noff = 0; offsets && noff < NAME_PACK_OFFSETS; noff++)
		if (!offsets[noff])
			break;
	app_off = noff;

	while (*nd) {
		unsigned len;
		unsigned int i;

		/* Search for a name to reuse */
		for (i = 0; i < noff; i++) {
			uint16_t ioff = offsets[i];
			if (match_at(pkt, ioff, nd)) {
				if (offset + 2 > pktsz)
					return 0;
				pkt[offset++] = (ioff >> 8) | 0xc0;
				pkt[offset++] = ioff & 0xff;
				return offset;
			}
		}

		/* Store the current label */
		len = *nd + 1;
		if (offset + len > pktsz)
			return 0;
		memcpy(pkt + offset, nd, len);

		if (offsets && app_off < NAME_PACK_OFFSETS && offset < 0xc000) {
			/* Record the new domain we've stored */
			offsets[app_off++] = offset;
			if (app_off < NAME_PACK_OFFSETS)
				offsets[app_off] = 0; /* terminate */
		}

		offset += len;
		nd += len;
	}
	if (offset + 1 > pktsz)
		return 0;
	pkt[offset++] = 0;
	return offset;
}
