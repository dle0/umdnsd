#pragma once

#include <inttypes.h>
#include <arpa/nameser.h>

#ifndef T_OPT
# define T_OPT  41
#endif
#ifndef T_NSEC
# define T_NSEC 47
#endif
#ifndef T_KX
# define T_KX   36 /* RFC 2230 */
#endif

#include "table.h"

/* A DNS resource record. */
struct rr {
	uint16_t rr_type;	/* T_* */
	uint16_t rr_class;	/* C_* */
	uint32_t rr_ttl;	/* cacheable lifetime in seconds */
	uint16_t rr_len;	/* length of rr_data[] */
	char     rr_data[];
};

/* Grows a rr to accommodate more data.
 * The pointer at rrp is adjusted with realloc().
 * Returns the pointer into rr_data of the newly-added data bytes.
 * Returns NULL on alloation error. */
void *rr_grow(struct rr **rrp, uint16_t size);

/* Grows and appends data to the rr data */
int rr_cat(struct rr **rrp, const void *data, uint16_t size);

/* Returns the allocation size of an rr */
size_t rr_size(const struct rr *rr);

/* Compares rr->rr_data against the RDATA at pkt[offset] */
int rr_eq_data(const char *pkt, uint16_t pktsz, uint16_t offset, uint16_t rdlength,
        const struct rr *rr, uint16_t *after_return);

#define rr_free(rr) free(rr)

/* Type and class tables */
extern const struct table rr_type_table[];	/* T_* */
extern const struct table rr_class_table[];	/* C_* */
