/*
 * Listen on an IPv4 mDNS multicast group
 */
#include <stdio.h>
#include <err.h>
#include <string.h>
#include <ifaddrs.h>
#include <unistd.h>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>

#include "mcast.h"
#include "verbose.h"

static const in_addr_t MDNS_ADDR = 0xe00000fb; /* 224.0.0.251 */
static const in_port_t MDNS_PORT = 5353;

/*
 * Joins the socket to the mDNS multicast group on the given interface.
 *  @param ifaddr  Address that identifies the interface.
 *  @param ifname  Display name of interface used in error messages.
 * Returns 0 on success, or -1 on error.
 */
static int
mcast_join(int fd, in_addr_t ifaddr, const char *ifname)
{
	struct ip_mreq mreq = {
		.imr_multiaddr.s_addr = htonl(MDNS_ADDR),
		.imr_interface.s_addr = ifaddr
	};
	int ret;

	if (verbose) {
		printf("IP_ADD_MEMBERSHIP %s %s\n", ifname,
			inet_ntoa(mreq.imr_multiaddr));
	}
	ret = setsockopt(fd, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof mreq);
	if (ret == -1)
		warn("IP_ADD_MEMBERSHIP %s %s", ifname,
			inet_ntoa(mreq.imr_interface));
	return ret;
}

/* Converts an interface name (eg "eth0") into
 * an IPv4 address (1.2.3.4).
 * This is used with IPv4 sockopts that need an
 * IPv4 address to identify the interface, not a name.
 * The name "default" converts to 0.0.0.0 (INADDR_ANY),
 * as a special case.
 * Returns 0 on success, -1 on error. */
static int
if_nametoaddr(const char *ifname, in_addr_t *ifaddr_return)
{
	struct ifaddrs *ifaddrs = NULL, *ifa;
	const struct sockaddr_in *sin;
	const struct in_addr *addr;
	int known = 0;
	int ret = -1;

	if (strcmp(ifname, "default") == 0) {
		*ifaddr_return = htonl(INADDR_ANY);
		return 0;
	}

	/* search system for matching interface names */
	if (getifaddrs(&ifaddrs) == -1)
		err(1, "getifaddrs");
	for (ifa = ifaddrs; ifa; ifa = ifa->ifa_next) {
		if (strcmp(ifname, ifa->ifa_name) != 0)
			continue;
		known++;
		if (ifa->ifa_addr->sa_family != AF_INET)
			continue;
		sin = (const struct sockaddr_in *)ifa->ifa_addr;
		addr = &sin->sin_addr;
		*ifaddr_return = addr->s_addr;
		ret = 0;
		break;
	}

	if (!known)
		warnx("%s: unknown interface", ifname);
	freeifaddrs(ifaddrs);
	return ret;
}

/* Creates an IPv4 multicast listener socket,
 * joined to mDNS group on the given interface.
 * Returns the socket file descriptor on success, or -1 on error. */
int
mcast_socket(const char *ifname)
{
	int fd;
	struct sockaddr_in sa = {
		.sin_family = AF_INET,
		.sin_port = htons(MDNS_PORT)
	};
	int val;
	in_addr_t ifaddr;

	if (if_nametoaddr(ifname, &ifaddr) == -1)
		return -1;

	fd = socket(AF_INET, SOCK_DGRAM, 0);
	if (fd == -1)
		err(1, "socket");

	if (mcast_join(fd, ifaddr, ifname) == -1) {
		close(fd);
		return -1;
	}

	/* Set IP TTL so that responses are only forwardable onto the local link */
	val = 255;
	if (setsockopt(fd, IPPROTO_IP, IP_TTL, &val, sizeof val) == -1)
		warn("IP_TTL");

	/* Bind and share the mDNS port with other processes */
	val = 1;
	if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &val, sizeof val) == -1)
		warn("SO_REUSEADDR");
	val = 1;
	if (setsockopt(fd, SOL_SOCKET, SO_REUSEPORT, &val, sizeof val) == -1)
		warn("SO_REUSEPORT");
	if (bind(fd, (const struct sockaddr *)&sa, sizeof sa) == -1)
		err(1, "bind port %u", ntohs(sa.sin_port));

	return fd;
}

int
mcast_send(int fd, const void *data, unsigned int datalen)
{
	const struct sockaddr_in to = {
		.sin_family = AF_INET,
		.sin_port = htons(MDNS_PORT),
		.sin_addr.s_addr = htonl(MDNS_ADDR),
	};

	return sendto(fd, data, datalen, 0,
		(const struct sockaddr *)&to, sizeof to);
}
