/*
 * Resource records: code tables and memory management
 */

#include <err.h>
#include <stdlib.h>
#include <string.h>

#include "table.h"
#include "rr.h"

const struct table rr_class_table[] = {
	{ C_IN, "IN" },
	{}
};

const struct table rr_type_table[] = {
	{ T_A,		"A" },
	{ T_AAAA,	"AAAA" },
	{ T_MX,		"MX" },
	{ T_TXT,	"TXT" },
	{ T_SRV,	"SRV" },
	{ T_PTR,	"PTR" },
	{ T_CNAME,	"CNAME" },
	{ T_NS,		"NS" },
	{ T_NSEC,	"NSEC" },
	{ T_OPT,	"OPT" },
	{ T_HINFO,	"HINFO" },
	/* T_SOA,       SOA records DO NOT exist in mDNS */
	{}
};

size_t
rr_size(const struct rr *rr)
{
	return (sizeof *rr) + rr->rr_len;
}

void *
rr_grow(struct rr **rrp, uint16_t size)
{
	struct rr *rr = *rrp;
	void *data;

	rr = realloc(rr, rr_size(rr) + size);
	if (!rr) {
		warn("realloc");
		return NULL;
	}
	data = &rr->rr_data[rr->rr_len];
	rr->rr_len += size;
	*rrp = rr;
	return data;
}

int
rr_cat(struct rr **rrp, const void *data, uint16_t size)
{
	void *dst = rr_grow(rrp, size);
	if (!dst)
		return -1;
	memcpy(dst, data, size);
	return 0;
}

