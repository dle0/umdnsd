#pragma once

#include "name.h"
#include "rr.h"

struct zone_rr {
	struct zone_rr *next;	/* singly-linked list */
	struct timespec time;	/* last time this <name,RR> was transmitted */
	const _DN char *name;   /* name associated with the record */
	struct rr rr;
};

/* Forgets all previously loaded zones. */
void zones_clear(void);

/* Loads zones from a file in "master file format".
 * Returns 0 on success, -1 on error */
int zones_load(const char *filename);

/* Return a linked list of zone records for the given name.
 * Returns NULL if the list is empty. Otherwise the list's tail
 * is the 'next' fields of the returned structure. */
struct zone_rr *zones_lookup(const _DN char *name);

/* Appends the RR to the resources list of the named zone.
 * The zone is created if it doesn't already exist.
 * The RR will be later freed by zones_clear().
 * Returns -1 on error (likely allocation). */
int zones_store(const _DN char *name, const struct rr *rr);

/* Iterates over all names in the zone.
 * Initialize iter to NULL before iterating.
 * Returns next name, or NULL on last name. */
const _DN char *zones_name_next(void **iter);

