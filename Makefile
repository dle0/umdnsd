CFLAGS = -Wall
CFLAGS += -g -O2
#CFLAGS += -Os -DNO_VERBOSE		# save space by removing verbose messages

PROG = umdnsd

SRCS  =
SRCS += load.c
SRCS += main.c
SRCS += mcast.c
SRCS += mcast6.c
SRCS += name.c
SRCS += recent.c
SRCS += recv.c
SRCS += respond.c
SRCS += rr.c
SRCS += table.c
SRCS += verbose.c
SRCS += zones.c

TESTS  =
TESTS += name-t
TESTS += table-t
TESTS += respond-t
TESTS += zones-t
TESTS += recent-t

#CFLAGS += -MMD
#-include *.d

OBJS += $(SRCS:.c=.o)
$(PROG): $(OBJS)
	$(LINK.c) -o $@ $(OBJS)

check: $(TESTS)
	for t in $(TESTS); do echo check $$t; ./$$t || exit; done
	@printf '%s: \033[7;32m PASS \033[m\n' $@

respond-t-SRCS = respond-t.c respond.c name.c table.c verbose.c rr.c
respond-t-SRCS += zones.c load.c
respond-t-OBJS = $(respond-t-SRCS:.c=.o)
respond-t: $(respond-t-OBJS)
	$(LINK.c) -o $@ $(respond-t-OBJS)

zones-t-SRCS = zones-t.c
zones-t-SRCS += name.c table.c verbose.c rr.c
zones-t-SRCS += zones.c load.c
zones-t-OBJS = $(zones-t-SRCS:.c=.o)
zones-t: $(zones-t-OBJS)
	$(LINK.c) -o $@ $(zones-t-OBJS)

recent-t-SRCS = recent-t.c
recent-t-SRCS += recent.c

clean:
	rm -f *.o *.d *.core
	rm -f $(PROG) $(TESTS)

