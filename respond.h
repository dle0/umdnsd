#pragma once

#include <inttypes.h>
#include <time.h>

struct query_info {
	unsigned int unicast : 1;	/* true when query srcaddr != 5353 */
	struct timespec time;		/* receipt time */
};

/* Computes response to an mDNS query packet.
 * The response is placed in the provided buffer.
 * Returns the length of the respond packet,
 * returns 0 if no response is possible. */
int mdns_respond(const void *pkt, uint16_t pktlen,
	void *buf, uint16_t bufsz, const struct query_info *qinfo);

