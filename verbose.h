#pragma once

#ifdef NO_VERBOSE
# define verbose 0		/* Save executable space by removing -v */
#else
extern int verbose;
#endif
